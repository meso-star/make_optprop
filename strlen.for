c     Copyright (C) 2008-2009 Vincent Eymet
c
c     Planet_EMC is free software; you can redistribute it and/or modify
c     it under the terms of the GNU General Public License as published by
c     the Free Software Foundation; either version 3, or (at your option)
c     any later version.

c     Planet_EMC is distributed in the hope that it will be useful,
c     but WITHOUT ANY WARRANTY; without even the implied warranty of
c     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
c     GNU General Public License for more details.

c     You should have received a copy of the GNU General Public License
c     along with Planet_EMC; if not, see <http://www.gnu.org/licenses/>
c
      integer function strlen(st)
      implicit none
c     
c     Purpose: to return the length (number of characters) in the
c     input character string
c
c     Input:
c       + st: character string
c     
c     Output:
c       + strlen: length of "st"
c
      integer i
      character st*(*)
      i=len(st)
      do while (st(i:i).eq.' ')
         i=i-1
      enddo
      strlen=i
      return
      end
