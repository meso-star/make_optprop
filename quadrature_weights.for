      subroutine quadrature_weights(
     &     Nb_lw,nu_lo_lw,nu_hi_lw,Ng_lw,w_lw_rrtm,
     &     Nb_sw,nu_lo_sw,nu_hi_sw,Ng_sw,w_sw_rrtm)
      implicit none
      include 'max.inc'
c     
c     Purpose: to compute rrtm quadrature weights
c     for both the SW and the LW
c     
c     Output:
c       + Nb_lw: number of LW intervals
c       + nu_lo_lw: lower wavenumber of each LW interval [cm^-1]
c       + nu_hi_lw: upper wavenumber of each LW interval [cm^-1]
c       + Ng_lw: quadrature order for each LW interval
c       + w_lw_rrtm: RRTM quadrature weights for each LW interval
c       + Nb_sw: number of SW intervals
c       + nu_lo_sw: lower wavenumber of each SW interval [cm^-1]
c       + nu_hi_sw: upper wavenumber of each SW interval [cm^-1]
c       + Ng_sw: quadrature order for each SW interval
c       + w_sw_rrtm: RRTM quadrature weights for each SW interval
c     
c     I/O
      integer Nb_lw
      double precision nu_lo_lw(1:16)
      double precision nu_hi_lw(1:16)
      integer Ng_lw(1:16)
      double precision w_lw_rrtm(1:16,1:20)
      integer Nb_sw
      double precision nu_lo_sw(1:14)
      double precision nu_hi_sw(1:14)
      integer Ng_sw(1:14)
      double precision w_sw_rrtm(1:14,1:20)
c     temp
      double precision wt_lw(1:16)
      double precision wt_sw(1:16)
c     label
      character*(Nchar_mx) label
      label='subroutine quadrature_weights'
      
! Number of spectral intervals for the LW
      Nb_lw=16
! Number of spectral intervals for the SW
      Nb_sw=14

! Number of g-points for each LW interval
      Ng_lw(1)=10
      Ng_lw(2)=12
      Ng_lw(3)=16
      Ng_lw(4)=14
      Ng_lw(5)=16
      Ng_lw(6)=8
      Ng_lw(7)=12
      Ng_lw(8)=8
      Ng_lw(9)=12
      Ng_lw(10)=6
      Ng_lw(11)=8
      Ng_lw(12)=8
      Ng_lw(13)=4
      Ng_lw(14)=2
      Ng_lw(15)=2
      Ng_lw(16)=2
! Lower wavenumber [cm^-1] limit for each LW interval
      nu_lo_lw(1)=10.0D+0
      nu_lo_lw(2)=350.0D+0
      nu_lo_lw(3)=500.0D+0
      nu_lo_lw(4)=630.0D+0
      nu_lo_lw(5)=700.0D+0
      nu_lo_lw(6)=820.0D+0
      nu_lo_lw(7)=980.0D+0
      nu_lo_lw(8)=1080.0D+0
      nu_lo_lw(9)=1180.0D+0
      nu_lo_lw(10)=1390.0D+0
      nu_lo_lw(11)=1480.0D+0
      nu_lo_lw(12)=1800.0D+0
      nu_lo_lw(13)=2080.0D+0
      nu_lo_lw(14)=2250.0D+0
      nu_lo_lw(15)=2380.0D+0
      nu_lo_lw(16)=2600.0D+0
! Upper wavenumber [cm^-1] limit for each LW interval
      nu_hi_lw(1)=350.0D+0
      nu_hi_lw(2)=500.0D+0
      nu_hi_lw(3)=630.0D+0
      nu_hi_lw(4)=700.0D+0
      nu_hi_lw(5)=820.0D+0
      nu_hi_lw(6)=980.0D+0
      nu_hi_lw(7)=1080.0D+0
      nu_hi_lw(8)=1180.0D+0
      nu_hi_lw(9)=1390.0D+0
      nu_hi_lw(10)=1480.0D+0
      nu_hi_lw(11)=1800.0D+0
      nu_hi_lw(12)=2080.0D+0
      nu_hi_lw(13)=2250.0D+0
      nu_hi_lw(14)=2380.0D+0
      nu_hi_lw(15)=2600.0D+0
      nu_hi_lw(16)=3250.0D+0
! RRTM 16-points quadrature weights
      wt_lw(1)=0.1527534276D+0
      wt_lw(2)=0.1491729617D+0
      wt_lw(3)=0.1420961469D+0
      wt_lw(4)=0.1316886544D+0
      wt_lw(5)=0.1181945205D+0
      wt_lw(6)=0.1019300893D+0
      wt_lw(7)=0.0832767040D+0
      wt_lw(8)=0.0626720116D+0
      wt_lw(9)=0.0424925D+0
      wt_lw(10)=0.0046269894D+0
      wt_lw(11)=0.0038279891D+0
      wt_lw(12)=0.0030260086D+0
      wt_lw(13)=0.0022199750D+0
      wt_lw(14)=0.0014140010D+0
      wt_lw(15)=0.000533D+0
      wt_lw(16)=0.000075D+0
! RRTM variable quadrature weights
! band 1
      w_lw_rrtm(1,1)=wt_lw(1)
      w_lw_rrtm(1,2)=wt_lw(2)
      w_lw_rrtm(1,3)=wt_lw(3)+wt_lw(4)
      w_lw_rrtm(1,4)=wt_lw(5)+wt_lw(6)
      w_lw_rrtm(1,5)=wt_lw(7)+wt_lw(8)
      w_lw_rrtm(1,6)=wt_lw(9)+wt_lw(10)
      w_lw_rrtm(1,7)=wt_lw(11)+wt_lw(12)
      w_lw_rrtm(1,8)=wt_lw(13)+wt_lw(14)
      w_lw_rrtm(1,9)=wt_lw(15)
      w_lw_rrtm(1,10)=wt_lw(16)
! band 2
      w_lw_rrtm(2,1)=wt_lw(1)
      w_lw_rrtm(2,2)=wt_lw(2)
      w_lw_rrtm(2,3)=wt_lw(3)
      w_lw_rrtm(2,4)=wt_lw(4)
      w_lw_rrtm(2,5)=wt_lw(5)
      w_lw_rrtm(2,6)=wt_lw(6)
      w_lw_rrtm(2,7)=wt_lw(7)
      w_lw_rrtm(2,8)=wt_lw(8)
      w_lw_rrtm(2,9)=wt_lw(9)+wt_lw(10)
      w_lw_rrtm(2,10)=wt_lw(11)+wt_lw(12)
      w_lw_rrtm(2,11)=wt_lw(13)+wt_lw(14)
      w_lw_rrtm(2,12)=wt_lw(15)+wt_lw(16)
! band 3
      w_lw_rrtm(3,1)=wt_lw(1)
      w_lw_rrtm(3,2)=wt_lw(2)
      w_lw_rrtm(3,3)=wt_lw(3)
      w_lw_rrtm(3,4)=wt_lw(4)
      w_lw_rrtm(3,5)=wt_lw(5)
      w_lw_rrtm(3,6)=wt_lw(6)
      w_lw_rrtm(3,7)=wt_lw(7)
      w_lw_rrtm(3,8)=wt_lw(8)
      w_lw_rrtm(3,9)=wt_lw(9)
      w_lw_rrtm(3,10)=wt_lw(10)
      w_lw_rrtm(3,11)=wt_lw(11)
      w_lw_rrtm(3,12)=wt_lw(12)
      w_lw_rrtm(3,13)=wt_lw(13)
      w_lw_rrtm(3,14)=wt_lw(14)
      w_lw_rrtm(3,15)=wt_lw(15)
      w_lw_rrtm(3,16)=wt_lw(16)
! band 4
      w_lw_rrtm(4,1)=wt_lw(1)
      w_lw_rrtm(4,2)=wt_lw(2)
      w_lw_rrtm(4,3)=wt_lw(3)
      w_lw_rrtm(4,4)=wt_lw(4)
      w_lw_rrtm(4,5)=wt_lw(5)
      w_lw_rrtm(4,6)=wt_lw(6)
      w_lw_rrtm(4,7)=wt_lw(7)
      w_lw_rrtm(4,8)=wt_lw(8)
      w_lw_rrtm(4,9)=wt_lw(9)
      w_lw_rrtm(4,10)=wt_lw(10)
      w_lw_rrtm(4,11)=wt_lw(11)
      w_lw_rrtm(4,12)=wt_lw(12)
      w_lw_rrtm(4,13)=wt_lw(13)
      w_lw_rrtm(4,14)=wt_lw(14)+wt_lw(15)+wt_lw(16)
! band 5
      w_lw_rrtm(5,1)=wt_lw(1)
      w_lw_rrtm(5,2)=wt_lw(2)
      w_lw_rrtm(5,3)=wt_lw(3)
      w_lw_rrtm(5,4)=wt_lw(4)
      w_lw_rrtm(5,5)=wt_lw(5)
      w_lw_rrtm(5,6)=wt_lw(6)
      w_lw_rrtm(5,7)=wt_lw(7)
      w_lw_rrtm(5,8)=wt_lw(8)
      w_lw_rrtm(5,9)=wt_lw(9)
      w_lw_rrtm(5,10)=wt_lw(10)
      w_lw_rrtm(5,11)=wt_lw(11)
      w_lw_rrtm(5,12)=wt_lw(12)
      w_lw_rrtm(5,13)=wt_lw(13)
      w_lw_rrtm(5,14)=wt_lw(14)
      w_lw_rrtm(5,15)=wt_lw(15)
      w_lw_rrtm(5,16)=wt_lw(16)
! band 6
      w_lw_rrtm(6,1)=wt_lw(1)+wt_lw(2)
      w_lw_rrtm(6,2)=wt_lw(3)+wt_lw(4)
      w_lw_rrtm(6,3)=wt_lw(5)+wt_lw(6)
      w_lw_rrtm(6,4)=wt_lw(7)+wt_lw(8)
      w_lw_rrtm(6,5)=wt_lw(9)+wt_lw(10)
      w_lw_rrtm(6,6)=wt_lw(11)+wt_lw(12)
      w_lw_rrtm(6,7)=wt_lw(13)+wt_lw(14)
      w_lw_rrtm(6,8)=wt_lw(15)+wt_lw(16)
! band 7
      w_lw_rrtm(7,1)=wt_lw(1)+wt_lw(2)
      w_lw_rrtm(7,2)=wt_lw(3)+wt_lw(4)
      w_lw_rrtm(7,3)=wt_lw(5)
      w_lw_rrtm(7,4)=wt_lw(6)
      w_lw_rrtm(7,5)=wt_lw(7)
      w_lw_rrtm(7,6)=wt_lw(8)
      w_lw_rrtm(7,7)=wt_lw(9)
      w_lw_rrtm(7,8)=wt_lw(10)
      w_lw_rrtm(7,9)=wt_lw(11)
      w_lw_rrtm(7,10)=wt_lw(12)
      w_lw_rrtm(7,11)=wt_lw(13)+wt_lw(14)
      w_lw_rrtm(7,12)=wt_lw(15)+wt_lw(16)
! band 8
      w_lw_rrtm(8,1)=wt_lw(1)+wt_lw(2)
      w_lw_rrtm(8,2)=wt_lw(3)+wt_lw(4)
      w_lw_rrtm(8,3)=wt_lw(5)+wt_lw(6)
      w_lw_rrtm(8,4)=wt_lw(7)+wt_lw(8)
      w_lw_rrtm(8,5)=wt_lw(9)+wt_lw(10)
      w_lw_rrtm(8,6)=wt_lw(11)+wt_lw(12)
      w_lw_rrtm(8,7)=wt_lw(13)+wt_lw(14)
      w_lw_rrtm(8,8)=wt_lw(15)+wt_lw(16)
! band 9
      w_lw_rrtm(9,1)=wt_lw(1)
      w_lw_rrtm(9,2)=wt_lw(2)
      w_lw_rrtm(9,3)=wt_lw(3)
      w_lw_rrtm(9,4)=wt_lw(4)
      w_lw_rrtm(9,5)=wt_lw(5)
      w_lw_rrtm(9,6)=wt_lw(6)
      w_lw_rrtm(9,7)=wt_lw(7)
      w_lw_rrtm(9,8)=wt_lw(8)
      w_lw_rrtm(9,9)=wt_lw(9)+wt_lw(10)
      w_lw_rrtm(9,10)=wt_lw(11)+wt_lw(12)
      w_lw_rrtm(9,11)=wt_lw(13)+wt_lw(14)
      w_lw_rrtm(9,12)=wt_lw(15)+wt_lw(16)
! band 10
      w_lw_rrtm(10,1)=wt_lw(1)+wt_lw(2)
      w_lw_rrtm(10,2)=wt_lw(3)+wt_lw(4)
      w_lw_rrtm(10,3)=wt_lw(5)+wt_lw(6)
      w_lw_rrtm(10,4)=wt_lw(7)+wt_lw(8)
      w_lw_rrtm(10,5)=wt_lw(9)+wt_lw(10)+wt_lw(11)+wt_lw(12)
      w_lw_rrtm(10,6)=wt_lw(13)+wt_lw(14)+wt_lw(15)+wt_lw(16)
! band 11
      w_lw_rrtm(11,1)=wt_lw(1)
      w_lw_rrtm(11,2)=wt_lw(2)
      w_lw_rrtm(11,3)=wt_lw(3)+wt_lw(4)
      w_lw_rrtm(11,4)=wt_lw(5)+wt_lw(6)
      w_lw_rrtm(11,5)=wt_lw(7)+wt_lw(8)
      w_lw_rrtm(11,6)=wt_lw(9)+wt_lw(10)
      w_lw_rrtm(11,7)=wt_lw(11)+wt_lw(12)+wt_lw(13)
      w_lw_rrtm(11,8)=wt_lw(14)+wt_lw(15)+wt_lw(16)
! band 12
      w_lw_rrtm(12,1)=wt_lw(1)
      w_lw_rrtm(12,2)=wt_lw(2)
      w_lw_rrtm(12,3)=wt_lw(3)
      w_lw_rrtm(12,4)=wt_lw(4)
      w_lw_rrtm(12,5)=wt_lw(5)+wt_lw(6)
      w_lw_rrtm(12,6)=wt_lw(7)+wt_lw(8)
      w_lw_rrtm(12,7)=wt_lw(9)+wt_lw(10)+wt_lw(11)+wt_lw(12)
      w_lw_rrtm(12,8)=wt_lw(13)+wt_lw(14)+wt_lw(15)+wt_lw(16)
! band 13
      w_lw_rrtm(13,1)=wt_lw(1)+wt_lw(2)+wt_lw(3)
      w_lw_rrtm(13,2)=wt_lw(4)+wt_lw(5)+wt_lw(6)
      w_lw_rrtm(13,3)=wt_lw(7)+wt_lw(8)+wt_lw(9)+wt_lw(10)
      w_lw_rrtm(13,4)=wt_lw(11)+wt_lw(12)+wt_lw(13)
     &     +wt_lw(14)+wt_lw(15)+wt_lw(16)
! band 14
      w_lw_rrtm(14,1)=wt_lw(1)+wt_lw(2)+wt_lw(3)+wt_lw(4)
     &     +wt_lw(5)+wt_lw(6)+wt_lw(7)+wt_lw(8)
      w_lw_rrtm(14,2)=wt_lw(9)+wt_lw(10)+wt_lw(11)+wt_lw(12)
     &     +wt_lw(13)+wt_lw(14)+wt_lw(15)+wt_lw(16)
! band 15
      w_lw_rrtm(15,1)=wt_lw(1)+wt_lw(2)+wt_lw(3)+wt_lw(4)
     &     +wt_lw(5)+wt_lw(6)+wt_lw(7)+wt_lw(8)
      w_lw_rrtm(15,2)=wt_lw(9)+wt_lw(10)+wt_lw(11)+wt_lw(12)
     &     +wt_lw(13)+wt_lw(14)+wt_lw(15)+wt_lw(16)
! band 16
      w_lw_rrtm(16,1)=wt_lw(1)+wt_lw(2)+wt_lw(3)+wt_lw(4)
      w_lw_rrtm(16,2)=wt_lw(5)+wt_lw(6)+wt_lw(7)+wt_lw(8)+wt_lw(9)
     &     +wt_lw(10)+wt_lw(11)+wt_lw(12)+wt_lw(13)+wt_lw(14)
     &     +wt_lw(15)+wt_lw(16)

! Number of g-points for each SW interval
      Ng_sw(1)=6
      Ng_sw(2)=12
      Ng_sw(3)=8
      Ng_sw(4)=8
      Ng_sw(5)=10
      Ng_sw(6)=10
      Ng_sw(7)=2
      Ng_sw(8)=10
      Ng_sw(9)=8
      Ng_sw(10)=6
      Ng_sw(11)=6
      Ng_sw(12)=8
      Ng_sw(13)=6
      Ng_sw(14)=12
! Lower wavenumber [cm^-1] limit for each SW interval
      nu_lo_sw(1)=2600.0D+0
      nu_lo_sw(2)=3250.0D+0
      nu_lo_sw(3)=4000.0D+0
      nu_lo_sw(4)=4650.0D+0
      nu_lo_sw(5)=5150.0D+0
      nu_lo_sw(6)=6150.0D+0
      nu_lo_sw(7)=7700.0D+0
      nu_lo_sw(8)=8050.0D+0
      nu_lo_sw(9)=12850.0D+0
      nu_lo_sw(10)=16000.0D+0
      nu_lo_sw(11)=22650.0D+0
      nu_lo_sw(12)=29000.0D+0
      nu_lo_sw(13)=38000.0D+0
      nu_lo_sw(14)=820.0D+0
! Upper wavenumber [cm^-1] limit for each SW interval
      nu_hi_sw(1)=3250.0D+0
      nu_hi_sw(2)=4000.0D+0
      nu_hi_sw(3)=4650.0D+0
      nu_hi_sw(4)=5150.0D+0
      nu_hi_sw(5)=6150.0D+0
      nu_hi_sw(6)=7700.0D+0
      nu_hi_sw(7)=8050.0D+0
      nu_hi_sw(8)=12850.0D+0
      nu_hi_sw(9)=16000.0D+0
      nu_hi_sw(10)=22650.0D+0
      nu_hi_sw(11)=29000.0D+0
      nu_hi_sw(12)=38000.0D+0
      nu_hi_sw(13)=50000.0D+0
      nu_hi_sw(14)=2600.0D+0
! RRTM 16-points quadrature weights
      wt_sw(1)=0.1527534276D+0
      wt_sw(2)=0.1491729617D+0
      wt_sw(3)=0.1420961469D+0
      wt_sw(4)=0.1316886544D+0
      wt_sw(5)=0.1181945205D+0
      wt_sw(6)=0.1019300893D+0
      wt_sw(7)=0.0832767040D+0
      wt_sw(8)=0.0626720116D+0
      wt_sw(9)=0.0424925000D+0
      wt_sw(10)=0.0046269894D+0
      wt_sw(11)=0.0038279891D+0
      wt_sw(12)=0.0030260086D+0
      wt_sw(13)=0.0022199750D+0
      wt_sw(14)=0.0014140010D+0
      wt_sw(15)=0.0005330000D+0
      wt_sw(16)=0.0000750000D+0
! RRTM variable quadrature weights
! band 1
      w_sw_rrtm(1,1)=wt_sw(1)+wt_sw(2)
      w_sw_rrtm(1,2)=wt_sw(3)+wt_sw(4)
      w_sw_rrtm(1,3)=wt_sw(5)+wt_sw(6)
      w_sw_rrtm(1,4)=wt_sw(7)+wt_sw(8)
      w_sw_rrtm(1,5)=wt_sw(9)+wt_sw(10)+wt_sw(11)+wt_sw(12)
      w_sw_rrtm(1,6)=wt_sw(13)+wt_sw(14)+wt_sw(15)+wt_sw(16)
! band 2
      w_sw_rrtm(2,1)=wt_sw(1)
      w_sw_rrtm(2,2)=wt_sw(2)
      w_sw_rrtm(2,3)=wt_sw(3)
      w_sw_rrtm(2,4)=wt_sw(4)
      w_sw_rrtm(2,5)=wt_sw(5)
      w_sw_rrtm(2,6)=wt_sw(6)+wt_sw(7)
      w_sw_rrtm(2,7)=wt_sw(8)
      w_sw_rrtm(2,8)=wt_sw(9)+wt_sw(10)
      w_sw_rrtm(2,9)=wt_sw(11)
      w_sw_rrtm(2,10)=wt_sw(12)+wt_sw(13)
      w_sw_rrtm(2,11)=wt_sw(14)
      w_sw_rrtm(2,12)=wt_sw(15)+wt_sw(16)
! band 3
      w_sw_rrtm(3,1)=wt_sw(1)
      w_sw_rrtm(3,2)=wt_sw(2)
      w_sw_rrtm(3,3)=wt_sw(3)
      w_sw_rrtm(3,4)=wt_sw(4)
      w_sw_rrtm(3,5)=wt_sw(5)+wt_sw(6)
      w_sw_rrtm(3,6)=wt_sw(7)+wt_sw(8)
      w_sw_rrtm(3,7)=wt_sw(9)+wt_sw(10)+wt_sw(11)+wt_sw(12)
      w_sw_rrtm(3,8)=wt_sw(13)+wt_sw(14)+wt_sw(15)+wt_sw(16)
! band 4
      w_sw_rrtm(4,1)=wt_sw(1)
      w_sw_rrtm(4,2)=wt_sw(2)
      w_sw_rrtm(4,3)=wt_sw(3)
      w_sw_rrtm(4,4)=wt_sw(4)
      w_sw_rrtm(4,5)=wt_sw(5)+wt_sw(6)
      w_sw_rrtm(4,6)=wt_sw(7)+wt_sw(8)
      w_sw_rrtm(4,7)=wt_sw(9)+wt_sw(10)+wt_sw(11)+wt_sw(12)
      w_sw_rrtm(4,8)=wt_sw(13)+wt_sw(14)+wt_sw(15)+wt_sw(16)
! band 5
      w_sw_rrtm(5,1)=wt_sw(1)
      w_sw_rrtm(5,2)=wt_sw(2)
      w_sw_rrtm(5,3)=wt_sw(3)
      w_sw_rrtm(5,4)=wt_sw(4)
      w_sw_rrtm(5,5)=wt_sw(5)
      w_sw_rrtm(5,6)=wt_sw(6)
      w_sw_rrtm(5,7)=wt_sw(7)
      w_sw_rrtm(5,8)=wt_sw(8)
      w_sw_rrtm(5,9)=wt_sw(9)+wt_sw(10)
      w_sw_rrtm(5,10)=wt_sw(11)+wt_sw(12)+wt_sw(13)+wt_sw(14)
     &     +wt_sw(15)+wt_sw(16)
! band 6
      w_sw_rrtm(6,1)=wt_sw(1)
      w_sw_rrtm(6,2)=wt_sw(2)
      w_sw_rrtm(6,3)=wt_sw(3)
      w_sw_rrtm(6,4)=wt_sw(4)
      w_sw_rrtm(6,5)=wt_sw(5)
      w_sw_rrtm(6,6)=wt_sw(6)
      w_sw_rrtm(6,7)=wt_sw(7)
      w_sw_rrtm(6,8)=wt_sw(8)
      w_sw_rrtm(6,9)=wt_sw(9)+wt_sw(10)
      w_sw_rrtm(6,10)=wt_sw(11)+wt_sw(12)+wt_sw(13)+wt_sw(14)
     &     +wt_sw(15)+wt_sw(16)
! band 7
      w_sw_rrtm(7,1)=wt_sw(1)+wt_sw(2)+wt_sw(3)+wt_sw(4)+wt_sw(5)
     &     +wt_sw(6)+wt_sw(7)+wt_sw(8)
      w_sw_rrtm(7,2)=wt_sw(9)+wt_sw(10)+wt_sw(11)+wt_sw(12)
     &     +wt_sw(13)+wt_sw(14)+wt_sw(15)+wt_sw(16)
! band 8
      w_sw_rrtm(8,1)=wt_sw(1)+wt_sw(2)
      w_sw_rrtm(8,2)=wt_sw(3)+wt_sw(4)
      w_sw_rrtm(8,3)=wt_sw(5)
      w_sw_rrtm(8,4)=wt_sw(6)
      w_sw_rrtm(8,5)=wt_sw(7)
      w_sw_rrtm(8,6)=wt_sw(8)
      w_sw_rrtm(8,7)=wt_sw(9)
      w_sw_rrtm(8,8)=wt_sw(10)
      w_sw_rrtm(8,9)=wt_sw(11)+wt_sw(12)
      w_sw_rrtm(8,10)=wt_sw(13)+wt_sw(14)+wt_sw(15)+wt_sw(16)
! band 9
      w_sw_rrtm(9,1)=wt_sw(1)+wt_sw(2)
      w_sw_rrtm(9,2)=wt_sw(3)+wt_sw(4)
      w_sw_rrtm(9,3)=wt_sw(5)+wt_sw(6)
      w_sw_rrtm(9,4)=wt_sw(7)+wt_sw(8)
      w_sw_rrtm(9,5)=wt_sw(9)+wt_sw(10)
      w_sw_rrtm(9,6)=wt_sw(11)+wt_sw(12)
      w_sw_rrtm(9,7)=wt_sw(13)+wt_sw(14)
      w_sw_rrtm(9,8)=wt_sw(15)+wt_sw(16)
! band 10
      w_sw_rrtm(10,1)=wt_sw(1)
      w_sw_rrtm(10,2)=wt_sw(2)
      w_sw_rrtm(10,3)=wt_sw(3)+wt_sw(4)
      w_sw_rrtm(10,4)=wt_sw(5)+wt_sw(6)
      w_sw_rrtm(10,5)=wt_sw(7)+wt_sw(8)+wt_sw(9)+wt_sw(10)
      w_sw_rrtm(10,6)=wt_sw(11)+wt_sw(12)+wt_sw(13)+wt_sw(14)
     &     +wt_sw(15)+wt_sw(16)
! band 11
      w_sw_rrtm(11,1)=wt_sw(1)
      w_sw_rrtm(11,2)=wt_sw(2)
      w_sw_rrtm(11,3)=wt_sw(3)+wt_sw(4)
      w_sw_rrtm(11,4)=wt_sw(5)+wt_sw(6)
      w_sw_rrtm(11,5)=wt_sw(7)+wt_sw(8)+wt_sw(9)+wt_sw(10)
      w_sw_rrtm(11,6)=wt_sw(11)+wt_sw(12)+wt_sw(13)+wt_sw(14)
     &     +wt_sw(15)+wt_sw(16)
! band 12
      w_sw_rrtm(12,1)=wt_sw(1)
      w_sw_rrtm(12,2)=wt_sw(2)
      w_sw_rrtm(12,3)=wt_sw(3)
      w_sw_rrtm(12,4)=wt_sw(4)
      w_sw_rrtm(12,5)=wt_sw(5)
      w_sw_rrtm(12,6)=wt_sw(6)
      w_sw_rrtm(12,7)=wt_sw(7)+wt_sw(8)+wt_sw(9)+wt_sw(10)
      w_sw_rrtm(12,8)=wt_sw(11)+wt_sw(12)+wt_sw(13)+wt_sw(14)
     &     +wt_sw(15)+wt_sw(16)
! band 13
      w_sw_rrtm(13,1)=wt_sw(1)
      w_sw_rrtm(13,2)=wt_sw(2)
      w_sw_rrtm(13,3)=wt_sw(3)+wt_sw(4)
      w_sw_rrtm(13,4)=wt_sw(5)+wt_sw(6)
      w_sw_rrtm(13,5)=wt_sw(7)+wt_sw(8)+wt_sw(9)+wt_sw(10)
      w_sw_rrtm(13,6)=wt_sw(11)+wt_sw(12)+wt_sw(13)+wt_sw(14)
     &     +wt_sw(15)+wt_sw(16)
! band 14
      w_sw_rrtm(14,1)=wt_sw(1)
      w_sw_rrtm(14,2)=wt_sw(2)
      w_sw_rrtm(14,3)=wt_sw(3)
      w_sw_rrtm(14,4)=wt_sw(4)
      w_sw_rrtm(14,5)=wt_sw(5)+wt_sw(6)
      w_sw_rrtm(14,6)=wt_sw(7)+wt_sw(8)
      w_sw_rrtm(14,7)=wt_sw(9)+wt_sw(10)
      w_sw_rrtm(14,8)=wt_sw(11)+wt_sw(12)
      w_sw_rrtm(14,9)=wt_sw(13)
      w_sw_rrtm(14,10)=wt_sw(14)
      w_sw_rrtm(14,11)=wt_sw(15)
      w_sw_rrtm(14,12)=wt_sw(16)

      return
      end
      
