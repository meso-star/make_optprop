subroutine read_nc_file(infile,outfile,Nlev,Nlay,pressure,Tsurf,temperature,height, &
     q_h2o,x_h2o,lw_emissivity,sw_emissivity,ka_lw,ka_sw,ks_sw)
  Use easy_netcdf
  implicit none
  include 'max.inc'
  !
  ! Purpose: to read a given netcdf file that was produced by ECRAD
  !
  !  Input:
  !     + infile: name of the ECRAD input file
  !     + oufile: name of the ECRAD output file (radiative properties)
  !     + Nlev: number of atmospheric levels
  !     + Nlay: number of atmospheric layers
  !     + pressure: pressure at each level [Pa]
  !     + Tsurf: temperature of the ground [K]
  !     + temperature: temperature at each level [K]
  !     + height: altitude at each level [m]
  !     + q_h2o: relative humidity for each layer [%]
  !     + x_h2o: molar fraction of H2O for each layer [mol of H2O / mol of mixture]
  !     + lw_emissivity: LW emissivity
  !     + sw_emissivity: SW emissivity
  !     + ka_lw: LW absorption coefficient
  !     + ka_sw: SW absorption coefficient
  !     + ks_sw: SW scattering coefficient
  !
  ! I/O
  character(len=511), intent(in) :: infile,outfile
  integer Nlev
  double precision pressure(1:Nlev_mx)
  double precision Tsurf
  double precision temperature(1:Nlev_mx)
  double precision height(1:Nlev_mx)
  double precision q_h2o(1:Nlay_mx)
  double precision x_h2o(1:Nlay_mx)
  double precision lw_emissivity,sw_emissivity
  double precision ka_lw(1:Ngpoints_lw_mx,1:Nlev_mx)
  double precision ka_sw(1:Ngpoints_sw_mx,1:Nlev_mx)
  double precision ks_sw(1:Ngpoints_sw_mx,1:Nlev_mx)
  ! temp
  type(netcdf_file) :: f
  integer :: iverbose
  logical :: is_write_mode,att_ex,generate_out_file
  character(len=511) :: var_name,att_name
  character(len=511) :: attr_str
  integer ::  ivarid,idim,ntmp
  integer :: xtype,ndims,Nvars,ngatts,recdim,rcode
  integer :: ndimlens(NF90_MAX_VAR_DIMS)
  integer :: ntotal,i,j,iatt,ib,ig,Nlay,ilay,ilev
  integer :: Ngpoints_lw,Ngpoints_sw
  logical :: is_present,ga_exist,do_transp
  integer, dimension(nf90_max_var_dims) :: dimIDs
  integer ncid,varid,Natts,istatus
  integer Nhl
  real(jprb), allocatable :: pressure_hl(:)
  real(jprb), allocatable :: temperature_hl(:)
  real(jprb), allocatable :: height_hl(:)
  real(jprb), allocatable :: q(:)
  real(jprb), allocatable :: oparam(:)
  real(jprb), allocatable :: od_lw(:,:)
  real(jprb), allocatable :: od_sw(:,:)
  real(jprb), allocatable :: ssa_sw(:,:)
  real(jprb), allocatable :: asymmetry_sw(:,:)
  real(jprb), allocatable :: ck_lw(:,:)
  real(jprb), allocatable :: ck_sw(:,:)
  real(jprb), allocatable :: ck_abs_sw(:,:)
  real(jprb), allocatable :: ck_sca_sw(:,:)
  real(jprb), allocatable :: od_abs_sw_tot(:)
  real(jprb) :: skin_temperature,sw_albedo
  double precision Mair,Mo3,Mh2o,R
  ! label
  character*(Nchar_mx) label
  label='subroutine read_nc_file'
  
  R=8.3144621D+0 ! J.K^-1.mol^-1
  Mair=28.965338D-3 ! kg.mol^-1 (molar mass of dry air)
  Mh2o=18.0153D-3 ! kg.mol^-1 (molar mass of H2O)
  Mo3=48.0D-3 ! kg.mol^-1

  iverbose=0
  is_write_mode=.false.
  call open_netcdf_file(f,infile,iverbose,is_write_mode)
  ncid=f%ncid
  call ncinq(ncid, ndims, Nvars, ngatts, recdim, rcode)

  ! Get only the list of variables
!!$  write(*,*) '----------------------------------------------'
!!$  write(*,*) 'File structure'
!!$  write(*,*) '----------------------------------------------'
!!$  write(*,*) 'Nvars=',Nvars
!!$  write(*,*)
  do varid=1,Nvars
     istatus=nf90_inquire_variable(ncid, varid, var_name, xtype, ndims, dimids, Natts)
     if (istatus.ne.NF90_NOERR) then
        write(*,*) 'Variable ',varid,' could not be found'
        goto 111
     endif

     !write(*,*) 'varid=',varid,' var_name= ',trim(var_name)!,' Natts=',Natts
     do iatt=1,Natts
        istatus=nf90_inq_attname(ncid, varid, iatt, att_name)
        if (istatus.ne.NF90_NOERR) then
           write(*,*) 'Attribute ',iatt,' could not be inquired'
           goto 222
        endif
        istatus=nf90_get_att(ncid, varid, att_name, attr_str)
        if (istatus.ne.NF90_NOERR) then
           write(*,*) 'Attribute ',iatt,' could not be get'
           goto 222
        endif
!!$        if (iatt.eq.2) then
!!$           write(*,*) trim(attr_str)
!!$        endif
222     continue
     enddo ! iatt
111  continue
!!$     write(*,*)
  enddo ! varid

!!$  write(*,*) '----------------------------------------------'
!!$  write(*,*) 'pressure_hl'
!!$  write(*,*) '----------------------------------------------'
  var_name='pressure_hl'
  is_present=exists(f,var_name)
  if (is_present) then
     call get_variable_id(f, var_name, ivarid)
!!$     write(*,*) trim(var_name),', ivarid=',ivarid
     call get_array_dimensions(f, ivarid, ndims, ndimlens, ntotal)
!!$     write(*,*) 'ndims=',ndims
     istatus = nf90_inquire_variable(ncid, ivarid, dimids = dimIDs)
     if (istatus.ne.NF90_NOERR) then
        write(*,*) 'Variable ',trim(var_name),'could not be inquired'
     endif
     do idim=1,ndims
        istatus = nf90_inquire_dimension(ncid, dimIDs(idim), len = ntmp)
        if (istatus.ne.NF90_NOERR) then
           write(*,*) 'Dimension ',idim,' of variable ',trim(var_name),'could not be inquired'
        else
!!$           write(*,*) 'dimension ',idim,' :',ntmp
        endif
        if (idim.eq.1) then
           Nhl=ntmp
        endif
     enddo ! idim
     call get_real_vector(f, var_name, pressure_hl) ! [pressure_hl]=Pa
     if (size(pressure_hl).ne.Nhl) then
        write(*,*) 'pressure_hl, Nhl=',Nhl
        write(*,*) 'size(pressure_hl)=',size(pressure_hl)
        stop
     endif
  endif
  Nlev=Nhl
  Nlay=Nhl-1
  if (Nlev.gt.Nlev_mx) then
     call error(label)
     write(*,*) 'Nlev=',Nlev
     write(*,*) 'while Nlev_mx=',Nlev_mx
     stop
  endif
  if (Nlay.gt.Nlay_mx) then
     call error(label)
     write(*,*) 'Nlay=',Nlay
     write(*,*) 'while Nlay_mx=',Nlay_mx
     stop
  endif
  do ilev=1,Nlev
     pressure(ilev)=pressure_hl(ilev)
  enddo ! ilev
  
!!$  write(*,*) '----------------------------------------------'
!!$  write(*,*) 'temperature_hl'
!!$  write(*,*) '----------------------------------------------'
  var_name='temperature_hl'
  is_present=exists(f,var_name)
  if (is_present) then
     call get_variable_id(f, var_name, ivarid)
!!$     write(*,*) trim(var_name),', ivarid=',ivarid
     call get_array_dimensions(f, ivarid, ndims, ndimlens, ntotal)
!!$     write(*,*) 'ndims=',ndims
     istatus = nf90_inquire_variable(ncid, ivarid, dimids = dimIDs)
     if (istatus.ne.NF90_NOERR) then
        write(*,*) 'Variable ',trim(var_name),'could not be inquired'
     endif
     do idim=1,ndims
        istatus = nf90_inquire_dimension(ncid, dimIDs(idim), len = ntmp)
        if (istatus.ne.NF90_NOERR) then
           write(*,*) 'Dimension ',idim,' of variable ',trim(var_name),'could not be inquired'
        else
!!$           write(*,*) 'dimension ',idim,' :',ntmp
        endif
     enddo ! idim
     call get_real_vector(f, var_name, temperature_hl) ! [temperature_hl]=Pa
     if (size(temperature_hl).ne.Nhl) then
        write(*,*) 'temperature_hl, Nhl=',Nhl
        write(*,*) 'size(temperature_hl)=',size(temperature_hl)
        stop
     endif
  endif
  do ilev=1,Nlev
     temperature(ilev)=temperature_hl(ilev)
  enddo ! ilev

!!$  write(*,*) '----------------------------------------------'
!!$  write(*,*) 'skin_temperature'
!!$  write(*,*) '----------------------------------------------'
  var_name='skin_temperature'
  is_present=exists(f,var_name)
  if (is_present) then
     call get_variable_id(f, var_name, ivarid)
!!$     write(*,*) trim(var_name),', ivarid=',ivarid
     call get_array_dimensions(f, ivarid, ndims, ndimlens, ntotal)
!!$     write(*,*) 'ndims=',ndims
     istatus = nf90_inquire_variable(ncid, ivarid, dimids = dimIDs)
     if (istatus.ne.NF90_NOERR) then
        write(*,*) 'Variable ',trim(var_name),'could not be inquired'
     endif
     do idim=1,ndims
        istatus = nf90_inquire_dimension(ncid, dimIDs(idim), len = ntmp)
        if (istatus.ne.NF90_NOERR) then
           write(*,*) 'Dimension ',idim,' of variable ',trim(var_name),'could not be inquired'
        else
!!$           write(*,*) 'dimension ',idim,' :',ntmp
        endif
     enddo ! idim
     call get_real_scalar(f, var_name, skin_temperature)
!!$     write(*,*) trim(var_name),':',skin_temperature
  endif
  Tsurf=skin_temperature

!!$  write(*,*) '----------------------------------------------'
!!$  write(*,*) 'height_hl'
!!$  write(*,*) '----------------------------------------------'
  var_name='height_hl'
  is_present=exists(f,var_name)
  if (is_present) then
     call get_variable_id(f, var_name, ivarid)
!!$     write(*,*) trim(var_name),', ivarid=',ivarid
     call get_array_dimensions(f, ivarid, ndims, ndimlens, ntotal)
!!$     write(*,*) 'ndims=',ndims
     istatus = nf90_inquire_variable(ncid, ivarid, dimids = dimIDs)
     if (istatus.ne.NF90_NOERR) then
        write(*,*) 'Variable ',trim(var_name),'could not be inquired'
     endif
     do idim=1,ndims
        istatus = nf90_inquire_dimension(ncid, dimIDs(idim), len = ntmp)
        if (istatus.ne.NF90_NOERR) then
           write(*,*) 'Dimension ',idim,' of variable ',trim(var_name),'could not be inquired'
        else
!!$           write(*,*) 'dimension ',idim,' :',ntmp
        endif
     enddo ! idim
     call get_real_vector(f, var_name, height_hl) ! [height_hl]=Pa
     if (size(height_hl).ne.Nhl) then
        write(*,*) 'height_hl, Nhl=',Nhl
        write(*,*) 'size(height_hl)=',size(height_hl)
        stop
     endif
  endif
  do ilev=1,Nlev
     height(ilev)=height_hl(ilev)
  enddo ! ilev

!!$  write(*,*) '----------------------------------------------'
!!$  write(*,*) 'q'
!!$  write(*,*) '----------------------------------------------'
  var_name='q'
  is_present=exists(f,var_name)
  if (is_present) then
     call get_variable_id(f, var_name, ivarid)
!!$     write(*,*) trim(var_name),', ivarid=',ivarid
     call get_array_dimensions(f, ivarid, ndims, ndimlens, ntotal)
!!$     write(*,*) 'ndims=',ndims
     istatus = nf90_inquire_variable(ncid, ivarid, dimids = dimIDs)
     if (istatus.ne.NF90_NOERR) then
        write(*,*) 'Variable ',trim(var_name),'could not be inquired'
     endif
     do idim=1,ndims
        istatus = nf90_inquire_dimension(ncid, dimIDs(idim), len = ntmp)
        if (istatus.ne.NF90_NOERR) then
           write(*,*) 'Dimension ',idim,' of variable ',trim(var_name),'could not be inquired'
        else
!!$           write(*,*) 'dimension ',idim,' :',ntmp
        endif
     enddo ! idim
     call get_real_vector(f, var_name, q) ! [q]=Pa
     if (size(q).ne.Nlay) then
        write(*,*) 'q, Nlay=',Nlay
        write(*,*) 'size(q)=',size(q)
        stop
     endif
  endif
  ! computation of x_h2o from q
  do ilay=1,Nlay
     q_h2o(ilay)=q(ilay)
     x_h2o(ilay)=q(ilay)/(q(ilay)+(1.0D+0-q(ilay))*Mh2o/Mair)
  enddo ! i


!!$  write(*,*) '----------------------------------------------'
!!$  write(*,*) 'lw_emissivity'
!!$  write(*,*) '----------------------------------------------'
  var_name='lw_emissivity'
  is_present=exists(f,var_name)
  if (is_present) then
     call get_variable_id(f, var_name, ivarid)
!!$     write(*,*) trim(var_name),', ivarid=',ivarid
     call get_array_dimensions(f, ivarid, ndims, ndimlens, ntotal)
!!$     write(*,*) 'ndims=',ndims
     istatus = nf90_inquire_variable(ncid, ivarid, dimids = dimIDs)
     if (istatus.ne.NF90_NOERR) then
        write(*,*) 'Variable ',trim(var_name),'could not be inquired'
     endif
     do idim=1,ndims
        istatus = nf90_inquire_dimension(ncid, dimIDs(idim), len = ntmp)
        if (istatus.ne.NF90_NOERR) then
           write(*,*) 'Dimension ',idim,' of variable ',trim(var_name),'could not be inquired'
        else
!!$           write(*,*) 'dimension ',idim,' :',ntmp
        endif
     enddo ! idim
     call get_real_scalar(f, var_name, lw_emissivity)
  endif

!!$  write(*,*) '----------------------------------------------'
!!$  write(*,*) 'sw_albedo'
!!$  write(*,*) '----------------------------------------------'
  var_name='sw_albedo'
  is_present=exists(f,var_name)
  if (is_present) then
     call get_variable_id(f, var_name, ivarid)
!!$     write(*,*) trim(var_name),', ivarid=',ivarid
     call get_array_dimensions(f, ivarid, ndims, ndimlens, ntotal)
!!$     write(*,*) 'ndims=',ndims
     istatus = nf90_inquire_variable(ncid, ivarid, dimids = dimIDs)
     if (istatus.ne.NF90_NOERR) then
        write(*,*) 'Variable ',trim(var_name),'could not be inquired'
     endif
     do idim=1,ndims
        istatus = nf90_inquire_dimension(ncid, dimIDs(idim), len = ntmp)
        if (istatus.ne.NF90_NOERR) then
           write(*,*) 'Dimension ',idim,' of variable ',trim(var_name),'could not be inquired'
        else
!!$           write(*,*) 'dimension ',idim,' :',ntmp
        endif
     enddo ! idim
     call get_real_scalar(f, var_name, sw_albedo)
  endif
  sw_emissivity=1.0D+0-sw_albedo

  call close_netcdf_file(f)

  iverbose=0
  is_write_mode=.false.
  call open_netcdf_file(f,outfile,iverbose,is_write_mode)
  ncid=f%ncid

  call ncinq(ncid, ndims, Nvars, ngatts, recdim, rcode)

  ! Get only the list of variables
!!$  write(*,*) '----------------------------------------------'
!!$  write(*,*) 'File structure'
!!$  write(*,*) '----------------------------------------------'
!!$  write(*,*) 'Nvars=',Nvars
!!$  write(*,*)
  do varid=1,Nvars
     istatus=nf90_inquire_variable(ncid, varid, var_name, xtype, ndims, dimids, Natts)
     if (istatus.ne.NF90_NOERR) then
        write(*,*) 'Variable ',varid,' could not be found'
        goto 333
     endif
!!$     write(*,*) 'varid=',varid,' var_name= ',trim(var_name)!,' Natts=',Natts
     do iatt=1,Natts
        istatus=nf90_inq_attname(ncid, varid, iatt, att_name)
        if (istatus.ne.NF90_NOERR) then
           write(*,*) 'Attribute ',iatt,' could not be inquired'
           goto 444
        endif
!!$        write(*,*) 'iatt=',iatt,' att_name=',trim(att_name)
        istatus=nf90_get_att(ncid, varid, att_name, attr_str)
        if (istatus.ne.NF90_NOERR) then
           write(*,*) 'Attribute ',iatt,' could not be get'
           goto 444
        endif
        !        if (iatt.eq.1) then
!!$        write(*,*) trim(attr_str)
        !        endif
444     continue
     enddo ! iatt
333  continue
!!$     write(*,*)
  enddo ! varid

!!$  write(*,*) '----------------------------------------------'
!!$  write(*,*) 'planck_hl'
!!$  write(*,*) '----------------------------------------------'
  var_name='planck_hl'
  is_present=exists(f, var_name)
  if (is_present) then
     call get_variable_id(f, var_name, ivarid)
!!$     write(*,*) trim(var_name),', ivarid=',ivarid
     call get_array_dimensions(f, ivarid, ndims, ndimlens, ntotal)
!!$     write(*,*) 'ndims=',ndims
     ! expected dimension
!!$     if (ndims.ne.3) then
!!$        write(*,*) 'pressure_hl, ndims=',ndims
!!$        write(*,*) 'expected: 3'
!!$        stop
!!$     endif
     istatus = nf90_inquire_variable(ncid, ivarid, dimids = dimIDs)
     if (istatus.ne.NF90_NOERR) then
        write(*,*) 'Variable ',trim(var_name),'could not be inquired'
     endif
     do idim=1,ndims
        istatus = nf90_inquire_dimension(ncid, dimIDs(idim), len = ntmp)
        if (istatus.ne.NF90_NOERR) then
           write(*,*) 'Dimension ',idim,' of variable ',trim(var_name),'could not be inquired'
        else
!!$           write(*,*) 'dimension ',idim,' :',ntmp
        endif
        if ((ntmp.gt.1).and.(ntmp.ne.Nhl)) then
           Ngpoints_lw=ntmp
!!$           write(*,*) '************************************************************'
!!$           write(*,*) 'Ngpoints_lw=',Ngpoints_lw
!!$           write(*,*) '************************************************************'
        endif
     enddo ! idim
     do_transp=.false.
!     call get_real_matrix(f, var_name, planck_hl, do_transp)
  endif
  if (Ngpoints_lw.gt.Ngpoints_lw_mx) then
     call error(label)
     write(*,*) 'Ngpoints_lw=',Ngpoints_lw
     write(*,*) 'while Ngpoints_lw_mx=',Ngpoints_lw_mx
     stop
  endif


!!$  write(*,*) '----------------------------------------------'
!!$  write(*,*) 'incoming_sw'
!!$  write(*,*) '----------------------------------------------'
  var_name='incoming_sw'
  is_present=exists(f, var_name)
  if (is_present) then
     call get_variable_id(f, var_name, ivarid)
!!$     write(*,*) trim(var_name),', ivarid=',ivarid
     call get_array_dimensions(f, ivarid, ndims, ndimlens, ntotal)
!!$     write(*,*) 'ndims=',ndims
     istatus = nf90_inquire_variable(ncid, ivarid, dimids = dimIDs)
     if (istatus.ne.NF90_NOERR) then
        write(*,*) 'Variable ',trim(var_name),'could not be inquired'
     endif
     do idim=1,ndims
        istatus = nf90_inquire_dimension(ncid, dimIDs(idim), len = ntmp)
        if (istatus.ne.NF90_NOERR) then
           write(*,*) 'Dimension ',idim,' of variable ',trim(var_name),'could not be inquired'
        else
!!$           write(*,*) 'dimension ',idim,' :',ntmp
        endif
        if ((ntmp.gt.1).and.(ntmp.ne.Nhl)) then
           Ngpoints_sw=ntmp
        endif
     enddo ! idim
!     call get_real_vector(f, var_name, incoming_sw) 
  endif
  if (Ngpoints_sw.gt.Ngpoints_sw_mx) then
     call error(label)
     write(*,*) 'Ngpoints_sw=',Ngpoints_sw
     write(*,*) 'while Ngpoints_sw_mx=',Ngpoints_sw_mx
     stop
  endif

!!$  write(*,*) '----------------------------------------------'
!!$  write(*,*) 'od_lw'
!!$  write(*,*) '----------------------------------------------'
  var_name='od_lw'
  is_present=exists(f, var_name)
  if (is_present) then
     call get_variable_id(f, var_name, ivarid)
!!$     write(*,*) trim(var_name),', ivarid=',ivarid
     call get_array_dimensions(f, ivarid, ndims, ndimlens, ntotal)
!!$     write(*,*) 'ndims=',ndims
     istatus = nf90_inquire_variable(ncid, ivarid, dimids = dimIDs)
     if (istatus.ne.NF90_NOERR) then
        write(*,*) 'Variable ',trim(var_name),'could not be inquired'
     endif
     do idim=1,ndims
        istatus = nf90_inquire_dimension(ncid, dimIDs(idim), len = ntmp)
        if (istatus.ne.NF90_NOERR) then
           write(*,*) 'Dimension ',idim,' of variable ',trim(var_name),'could not be inquired'
        else
!!$           write(*,*) 'dimension ',idim,' :',ntmp
        endif
     enddo ! idim
     do_transp=.false.
     call get_real_matrix(f, var_name, od_lw, do_transp)
  endif
  do ig=1,Ngpoints_lw
     do ilay=1,Nlay
        ka_lw(ig,ilay)=(od_lw(ig,ilay))/(height_hl(ilay)-height_hl(ilay+1))
     enddo ! i
  enddo ! ig


!!$  write(*,*) '----------------------------------------------'
!!$  write(*,*) 'od_sw'
!!$  write(*,*) '----------------------------------------------'
  var_name='od_sw'
  is_present=exists(f, var_name)
  if (is_present) then
     call get_variable_id(f, var_name, ivarid)
!!$     write(*,*) trim(var_name),', ivarid=',ivarid
     call get_array_dimensions(f, ivarid, ndims, ndimlens, ntotal)
!!$     write(*,*) 'ndims=',ndims
     istatus = nf90_inquire_variable(ncid, ivarid, dimids = dimIDs)
     if (istatus.ne.NF90_NOERR) then
        write(*,*) 'Variable ',trim(var_name),'could not be inquired'
     endif
     do idim=1,ndims
        istatus = nf90_inquire_dimension(ncid, dimIDs(idim), len = ntmp)
        if (istatus.ne.NF90_NOERR) then
           write(*,*) 'Dimension ',idim,' of variable ',trim(var_name),'could not be inquired'
        else
!!$           write(*,*) 'dimension ',idim,' :',ntmp
        endif
     enddo ! idim
     do_transp=.false.
     call get_real_matrix(f, var_name, od_sw, do_transp)
  endif


!!$  write(*,*) '----------------------------------------------'
!!$  write(*,*) 'ssa_sw'
!!$  write(*,*) '----------------------------------------------'
  var_name='ssa_sw'
  is_present=exists(f, var_name)
  if (is_present) then
     call get_variable_id(f, var_name, ivarid)
!!$     write(*,*) trim(var_name),', ivarid=',ivarid
     call get_array_dimensions(f, ivarid, ndims, ndimlens, ntotal)
!!$     write(*,*) 'ndims=',ndims
     istatus = nf90_inquire_variable(ncid, ivarid, dimids = dimIDs)
     if (istatus.ne.NF90_NOERR) then
        write(*,*) 'Variable ',trim(var_name),'could not be inquired'
     endif
     do idim=1,ndims
        istatus = nf90_inquire_dimension(ncid, dimIDs(idim), len = ntmp)
        if (istatus.ne.NF90_NOERR) then
           write(*,*) 'Dimension ',idim,' of variable ',trim(var_name),'could not be inquired'
        else
!!$           write(*,*) 'dimension ',idim,' :',ntmp
        endif
     enddo ! idim
     do_transp=.false.
     call get_real_matrix(f, var_name, ssa_sw, do_transp)
  endif

!!$  write(*,*) '----------------------------------------------'
!!$  write(*,*) 'asymmetry_sw'
!!$  write(*,*) '----------------------------------------------'
  var_name='asymmetry_sw'
  is_present=exists(f, var_name)
  if (is_present) then
     call get_variable_id(f, var_name, ivarid)
!!$     write(*,*) trim(var_name),', ivarid=',ivarid
     call get_array_dimensions(f, ivarid, ndims, ndimlens, ntotal)
!!$     write(*,*) 'ndims=',ndims
     istatus = nf90_inquire_variable(ncid, ivarid, dimids = dimIDs)
     if (istatus.ne.NF90_NOERR) then
        write(*,*) 'Variable ',trim(var_name),'could not be inquired'
     endif
     do idim=1,ndims
        istatus = nf90_inquire_dimension(ncid, dimIDs(idim), len = ntmp)
        if (istatus.ne.NF90_NOERR) then
           write(*,*) 'Dimension ',idim,' of variable ',trim(var_name),'could not be inquired'
        else
!!$           write(*,*) 'dimension ',idim,' :',ntmp
        endif
     enddo ! idim
     do_transp=.false.
     call get_real_matrix(f, var_name, asymmetry_sw, do_transp)
  endif

  call close_netcdf_file(f)

  ! Compute ck_sw
  do ig=1,Ngpoints_sw
     do ilay=1,Nlay
        ka_sw(ig,ilay)=(od_sw(ig,ilay))/(height_hl(ilay)-height_hl(ilay+1))*(1.0-ssa_sw(ig,ilay))
        ks_sw(ig,ilay)=(od_sw(ig,ilay))/(height_hl(ilay)-height_hl(ilay+1))*ssa_sw(ig,ilay)
     enddo ! i
  enddo ! ig
  
  return
end subroutine read_nc_file
