      subroutine generate_opt_prop_file(outfile,Nw,Nlev,Nlay,
     &     Tsurf,pressure,temperature,height,
     &     x_h2o_nominal,x_h2o_water,
     &     lw_emissivity,sw_emissivity,
     &     Nb_lw,nu_lo_lw,nu_hi_lw,Ng_lw,w_lw_rrtm,
     &     Nb_sw,nu_lo_sw,nu_hi_sw,Ng_sw,w_sw_rrtm,
     &     ka_lw_nominal,ka_lw_water,
     &     ka_sw_nominal,ka_sw_water,
     &     ks_sw_nominal,ks_sw_water)
      implicit none
      include 'max.inc'
      include 'formats.inc'
c     
c     Purpose: to generate the optical properties file for the atmosphere
c     
c     Input:
c     + outfile: file to generate
c     + Nw: number of points of water vapor concentration
c     + Nlev: number of atmospheric levels
c     + Nlay: number of atmospheric layers
c     + Tsurf: temperature of the ground [K]
c     + pressure: pressure at each level [Pa]
c     + temperature: temperature at each level [K]
c     + height: altitude at each level [m]
c     + x_h2o_nominal: molar fraction of H2O at each level, for the nominal profile
c     + x_h2o_water: molar fraction of H2O at each level, for the "water" profile
c     + lw_emissivity: LW emissivity
c     + sw_emissivity: SW emissivity
c     + Nb_lw: number of LW spectral intervals
c     + nu_lo_lw: lower wavenumber limit of each LW interval [cm^-1]
c     + nu_hi_lw: upper wavenumber limit of each LW interval [cm^-1]
c     + Ng_lw: number of quadrature points in each LW interval
c     + w_lw_rrtm: quadrature weights for each LW interval
c     + Nb_sw: number of SW spectral intervals
c     + nu_lo_sw: lower wavenumber limit of each SW interval [cm^-1]
c     + nu_hi_sw: upper wavenumber limit of each SW interval [cm^-1]
c     + Ng_sw: number of quadrature points in each SW interval
c     + w_sw_rrtm: quadrature weights for each SW interval
c     + ka_lw_nominal: LW absorption coefficient [m^-1] (nominal profile)
c     + ka_lw_water: LW absorption coefficient [m^-1] (water profile)
c     + ka_sw_nominal: SW absorption coefficient [m^-1] (nominal profile)
c     + ka_sw_water: SW absorption coefficient [m^-1] (water profile)
c     + ks_sw_nominal: SW scattering coefficient [m^-1] (nominal profile)
c     + ks_sw_water: SW scattering coefficient [m^-1] (water profile)
c     
c     I/O
      character*(Nchar_mx) outfile
      integer Nw,Nlev,Nlay
      double precision pressure(1:Nlev_mx)
      double precision Tsurf
      double precision temperature(1:Nlev_mx)
      double precision height(1:Nlev_mx)
      double precision x_h2o_nominal(1:Nlay_mx)
      double precision x_h2o_water(1:Nlay_mx,1:Nw_mx)
      double precision lw_emissivity,sw_emissivity
      double precision ka_lw_nominal(1:Ngpoints_lw_mx,1:Nlev_mx)
      double precision ka_lw_water(1:Ngpoints_lw_mx,1:Nlev_mx,1:Nw_mx)
      double precision ka_sw_nominal(1:Ngpoints_sw_mx,1:Nlev_mx)
      double precision ka_sw_water(1:Ngpoints_sw_mx,1:Nlev_mx,1:Nw_mx)
      double precision ks_sw_nominal(1:Ngpoints_sw_mx,1:Nlev_mx)
      double precision ks_sw_water(1:Ngpoints_sw_mx,1:Nlev_mx,1:Nw_mx)
      integer Nb_lw
      double precision nu_lo_lw(1:16)
      double precision nu_hi_lw(1:16)
      integer Ng_lw(1:16)
      double precision w_lw_rrtm(1:16,1:20)
      integer Nb_sw
      double precision nu_lo_sw(1:14)
      double precision nu_hi_sw(1:14)
      integer Ng_sw(1:14)
      double precision w_sw_rrtm(1:14,1:20)
c     temp
      integer ilay,ilev,ib,ig,i,iw
      character*(Nchar_mx) outfile2
c     label
      character*(Nchar_mx) label
      label='subroutine generate_opt_prop_file'
      
      open(11,file=trim(outfile))
      write(11,'(a)') '# Number of levels:'
      write(11,*) Nlev
      write(11,'(a)') '# Number of layers:'
      write(11,*) Nlay
      write(11,'(a)') '# Ground temperature:'
      write(11,*) Tsurf
      write(11,'(a)') '# Pressure [Pa]'
      do ilev=Nlev,1,-1
         write(11,*) pressure(ilev)
      enddo                     ! ilev
      write(11,'(a)') '# Temperature [K]'
      do ilev=Nlev,1,-1
         write(11,*) temperature(ilev)
      enddo                     ! ilev
      write(11,'(a)') '# Altitude [m]'
      do ilev=Nlev,1,-1
         write(11,*) height(ilev)
      enddo                     ! ilev
      write(11,'(a)') '# Nominal x(H2O) [mol h2o / mol of gaz mixture]'
      do ilay=Nlay,1,-1
         write(11,*) x_h2o_nominal(ilay)
      enddo                     ! ilev
      write(11,'(a)') '# Number of water vapor concentration values'
      write(11,*) Nw
      do iw=1,Nw
         write(11,'(a,i3)') '# x(H2O) [mol h2o / mol of gaz mixture]'
     &        //' for point index ',iw
         do ilay=Nlay,1,-1
            write(11,*) x_h2o_water(ilay,iw)
         enddo                  ! ilev
      enddo                     ! iw
      write(11,'(a)') '# LW emissivity'
      write(11,*) lw_emissivity
      write(11,'(a)') '# SW emissivity'
      write(11,*) sw_emissivity
      write(11,'(a)') '# Number of spectral intervals (LW)'
      write(11,*) Nb_lw
      write(11,'(a)') '# Number of spectral intervals (SW)'
      write(11,*) Nb_sw
      do ib=1,Nb_lw
         write(11,'(a,i3)') '# LW interval index:',ib
         write(11,'(a)') '# Lower wavenumber [cm^-1]:'
         write(11,*) nu_lo_lw(ib)
         write(11,'(a)') '# Upper wavenumber [cm^-1]:'
         write(11,*) nu_hi_lw(ib)
         write(11,'(a)') '# Number of quadrature points:'
         write(11,*) Ng_lw(ib)
         write(11,'(a)') '# Quadrature weights:'
         do ig=1,Ng_lw(ib)
            write(11,*) w_lw_rrtm(ib,ig)
         enddo                  ! ig
      enddo                     ! ib
c     SW: per interval
      ib=Nb_sw
      write(11,'(a,i3)') '# SW interval index:',1
      write(11,'(a)') '# Lower wavenumber [cm^-1]:'
      write(11,*) nu_lo_sw(ib)
      write(11,'(a)') '# Upper wavenumber [cm^-1]:'
      write(11,*) nu_hi_sw(ib)
      write(11,'(a)') '# Number of quadrature points:'
      write(11,*) Ng_sw(ib)
      write(11,'(a)') '# Quadrature weights:'
      do ig=1,Ng_sw(ib)
         write(11,*) w_sw_rrtm(ib,ig)
      enddo                     ! ig
      do ib=1,Nb_sw-1
         write(11,'(a,i3)') '# SW interval index:',ib+1
         write(11,'(a)') '# Lower wavenumber [cm^-1]:'
         write(11,*) nu_lo_sw(ib)
         write(11,'(a)') '# Upper wavenumber [cm^-1]:'
         write(11,*) nu_hi_sw(ib)
         write(11,'(a)') '# Number of quadrature points:'
         write(11,*) Ng_sw(ib)
         write(11,'(a)') '# Quadrature weights:'
         do ig=1,Ng_sw(ib)
            write(11,*) w_sw_rrtm(ib,ig)
         enddo                  ! ig
      enddo                     ! ib
      i=0
      do ib=1,Nb_lw
         do ig=1,Ng_lw(ib)
            i=i+1
            write(11,'(a,i3,a,i3)') '# Nominal absorption coefficient'
     &           //' [m^-1] for LW interval: ',ib,' ; g-point: ',ig
            do ilay=Nlay,1,-1
               if (ka_lw_nominal(i,ilay).lt.0.0D+0) then
                  write(*,*) 'ka_lw_nominal(',i,',',ilay,')=',
     &                 ka_lw_nominal(i,ilay)
                  ka_lw_nominal(i,ilay)=0.0D+0
               endif
               write(11,*) ka_lw_nominal(i,ilay)
            enddo               ! ilay
            do iw=1,Nw
               write(11,'(a,i3,a,i3,a,i3)') '# Water absorption'
     &              //' coefficient'
     &              //' [m^-1] for LW interval: ',ib,' ; g-point: ',ig,
     &              ' ; x-point:',iw
               do ilay=Nlay,1,-1
                  if (ka_lw_water(i,ilay,iw).lt.0.0D+0) then
                     write(*,*) 'ka_lw_water(',i,',',ilay,',',iw,')=',
     &                    ka_lw_water(i,ilay,iw)
                     ka_lw_water(i,ilay,iw)=0.0D+0
                  endif
                  write(11,*) ka_lw_water(i,ilay,iw)
               enddo            ! ilay
            enddo               ! iw
         enddo                  ! ig
      enddo                     ! ib
c     SW: per interval, per g-point
      i=0
      do ib=1,Nb_sw-1
         do ig=1,Ng_sw(ib)
            i=i+1
         enddo                  ! ig
      enddo                     ! ib
c     ABSORBTION coefficient
      ib=Nb_sw                  ! last interval first
      do ig=1,Ng_sw(ib)
         i=i+1
         write(11,'(a,i3,a,i3)') '# Nominal absorption coefficient'
     &        //' [m^-1] for SW interval: ',1,' ; g-point: ',ig
         do ilay=Nlay,1,-1
               if (ka_sw_nominal(i,ilay).lt.0.0D+0) then
                  write(*,*) 'ka_sw_nominal(',i,',',ilay,')=',
     &                 ka_sw_nominal(i,ilay)
                  ka_sw_nominal(i,ilay)=0.0D+0
               endif
            write(11,*) ka_sw_nominal(i,ilay)
         enddo                  ! ilay
         do iw=1,Nw
            write(11,'(a,i3,a,i3,a,i3)') '# Water absorption'
     &           //' coefficient'
     &           //' [m^-1] for SW interval: ',1,' ; g-point: ',ig,
     &           ' ; x-point:',iw
            do ilay=Nlay,1,-1
               if (ka_sw_water(i,ilay,iw).lt.0.0D+0) then
!                  write(*,*) 'ka_sw_water(',i,',',ilay,',',iw,')=',
!     &                 ka_sw_water(i,ilay,iw)
                  ka_sw_water(i,ilay,iw)=0.0D+0
               endif
               write(11,*) ka_sw_water(i,ilay,iw)
            enddo               ! ilay
         enddo                  ! iw
      enddo                     ! ig
c     Then all other intervals
      i=0
      do ib=1,Nb_sw-1
         do ig=1,Ng_sw(ib)
            i=i+1
            write(11,'(a,i3,a,i3)') '# Nominal absorption coefficient'
     &           //' [m^-1] for SW interval: ',ib+1,' ; g-point: ',ig
            do ilay=Nlay,1,-1
               if (ka_sw_nominal(i,ilay).lt.0.0D+0) then
                  write(*,*) 'ka_sw_nominal(',i,',',ilay,')=',
     &                 ka_sw_nominal(i,ilay)
                  ka_sw_nominal(i,ilay)=0.0D+0
               endif
               write(11,*) ka_sw_nominal(i,ilay)
            enddo               ! ilay
            do iw=1,Nw
               write(11,'(a,i3,a,i3,a,i3)') '# Water absorption '
     &              //'coefficient'
     &              //' [m^-1] for SW interval: ',ib+1,
     &              ' ; g-point: ',ig,
     &              ' ; x-point:',iw
               do ilay=Nlay,1,-1
                  if (ka_sw_water(i,ilay,iw).lt.0.0D+0) then
!                     write(*,*) 'ka_sw_water(',i,',',ilay,',',iw,')=',
!     &                    ka_sw_water(i,ilay,iw)
                     ka_sw_water(i,ilay,iw)=0.0D+0
                  endif
                  write(11,*) ka_sw_water(i,ilay,iw)
               enddo            ! ilay
            enddo               ! iw
         enddo                  ! ig
      enddo                     ! ib
c     SCATTERING coefficient
      ib=Nb_sw                  ! last interval first
      do ig=1,Ng_sw(ib)
         i=i+1
         write(11,'(a,i3,a,i3)') '# Nominal scattering coefficient'
     &        //' [m^-1] for SW interval: ',1,' ; g-point: ',ig
         do ilay=Nlay,1,-1
            if (ks_sw_nominal(i,ilay).lt.0.0D+0) then
               write(*,*) 'ks_sw_nominal(',i,',',ilay,')=',
     &              ks_sw_nominal(i,ilay)
               ks_sw_nominal(i,ilay)=0.0D+0
            endif
            write(11,*) ks_sw_nominal(i,ilay)
         enddo                  ! ilay
         do iw=1,Nw
            write(11,'(a,i3,a,i3,a,i3)') '# Water scattering '
     &           //'coefficient'
     &           //' [m^-1] for SW interval: ',1,
     &              ' ; g-point: ',ig,
     &              ' ; x-point:',iw
            do ilay=Nlay,1,-1
               if (ks_sw_water(i,ilay,iw).lt.0.0D+0) then
                  write(*,*) 'ks_sw_water(',i,',',ilay,',',iw,')=',
     &                 ks_sw_water(i,ilay,iw)
                  ks_sw_water(i,ilay,iw)=0.0D+0
               endif
               write(11,*) ks_sw_water(i,ilay,iw)
            enddo               ! ilay
         enddo                  ! iw
      enddo                     ! ig
c     Then all other intervals
      i=0
      do ib=1,Nb_sw-1
         do ig=1,Ng_sw(ib)
            i=i+1
            write(11,'(a,i3,a,i3)') '# Nominal scattering coefficient'
     &           //' [m^-1] for SW interval: ',ib+1,' ; g-point: ',ig
            do ilay=Nlay,1,-1
               if (ks_sw_nominal(i,ilay).lt.0.0D+0) then
                  write(*,*) 'ks_sw_nominal(',i,',',ilay,')=',
     &                 ks_sw_nominal(i,ilay)
                  ks_sw_nominal(i,ilay)=0.0D+0
               endif
               write(11,*) ks_sw_nominal(i,ilay)
            enddo               ! ilay
            do iw=1,Nw
               write(11,'(a,i3,a,i3,a,i3)') '# Water scattering '
     &              //'coefficient'
     &              //' [m^-1] for SW interval: ',ib+1,
     &              ' ; g-point: ',ig,
     &              ' ; x-point:',iw
               do ilay=Nlay,1,-1
                  if (ks_sw_water(i,ilay,iw).lt.0.0D+0) then
                     write(*,*) 'ks_sw_water(',i,',',ilay,',',iw,')=',
     &                    ks_sw_water(i,ilay,iw)
                     ks_sw_water(i,ilay,iw)=0.0D+0
                  endif
                  write(11,*) ks_sw_water(i,ilay,iw)
               enddo            ! ilay
            enddo ! iw
         enddo                  ! ig
      enddo                     ! ib
      close(11)
      
      write(*,*) 'File was generated: ',trim(outfile)


c     Debug
      outfile2='./ka_lw.txt'
      open(12,file=trim(outfile2))
      ilay=1
      do iw=1,Nw
         write(12,48) x_h2o_water(ilay,iw),
     &        (ka_lw_water(i,ilay,iw)
     &        ,i=1,Ng_lw(1))
      enddo ! iw
      close(12)
      write(*,*) 'File was generated: ',trim(outfile2)
      
      outfile2='./ka_sw.txt'
      open(12,file=trim(outfile2))
      ilay=1
      do iw=1,Nw
         write(12,48) x_h2o_water(ilay,iw),
     &        (ka_sw_water(i,ilay,iw)
     &        ,i=1,Ng_sw(1))
      enddo ! iw
      close(12)
      write(*,*) 'File was generated: ',trim(outfile2)
      
      outfile2='./ks_sw.txt'
      open(12,file=trim(outfile2))
      ilay=1
      do iw=1,Nw
         write(12,48) x_h2o_water(ilay,iw),
     &        (ks_sw_water(i,ilay,iw)
     &        ,i=1,Ng_sw(1))
      enddo ! iw
      close(12)
      write(*,*) 'File was generated: ',trim(outfile2)
c     Debug
      
      return
      end
      
