      subroutine exec(command)
      implicit none
      include 'max.inc'
c
c     Purpose: to execute the system command defined in the "command" character string
c
c     Input:
c       + command: character string that defines the system command to execute
c         (example: command='cp toto1 toto2')
c
      integer strlen
      character*(Nchar_mx) command,label
      label='subroutine exec'

      call system(command(1:strlen(command)))

      return
      end
