          integer Nmat_mx
	  integer Nb_mx
	  integer Nb_lw_mx
	  integer Nb_sw_mx
	  integer Nlev_mx
	  integer Nlay_mx
	  integer Ngpoints_lw_mx
	  integer Ngpoints_sw_mx
	  integer Nw_mx
	  integer Nchar_mx

	  parameter(Nmat_mx=3)
	  parameter(Nb_mx=100)
	  parameter(Nb_lw_mx=16)
	  parameter(Nb_sw_mx=14)
	  parameter(Nlev_mx=201)
	  parameter(Nlay_mx=Nlev_mx-1)
	  parameter(Ngpoints_lw_mx=200)
	  parameter(Ngpoints_sw_mx=200)
	  parameter(Nw_mx=100)
	  parameter(Nchar_mx=1000)
