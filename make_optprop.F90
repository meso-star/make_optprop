program make_optprop
  Use easy_netcdf
  ! Purpose: to open a given netcdf file, and replace some
  ! variables by some value.
  ! Warning: the structure of the data must be known
  
  implicit none
  include 'max.inc'
  character*(Nchar_mx) path_to_original,original_file
  character*(Nchar_mx) path_to_modified,modified_file
  character*(Nchar_mx) path_to_output
  character*(Nchar_mx) nowater_file,water_file,nominal_file
  character*(Nchar_mx) file1,file2,command
  logical dir_ex,file_ex
  !    
  type(netcdf_file) :: f
  character(len=511) :: in_file_name
  integer :: iverbose
  logical :: is_write_mode,att_ex
  character(len=511) :: var_name,att_name
  character(len=511) :: attr_str
  integer ::  ivarid
  integer :: Nvar,ivar
  character(len=511), dimension(nf90_max_var_dims) :: var_list
  double precision, dimension(nf90_max_var_dims) :: values
  integer :: xtype,ndims,ngatts,recdim,rcode
  integer :: ndimlens(NF90_MAX_VAR_DIMS)
  integer :: ntotal,Nlevels,Nc,i,iatt
  logical :: is_present,ga_exist
  integer, dimension(nf90_max_var_dims) :: dimIDs
  integer ncid,varid,Natts,istatus
  integer iw,Nw
  double precision q_min,q_max,q
  double precision exp_min,exp_max,exp_iw
  character*3 str3
  !  
  logical generate_resfile
  character(len=511) :: infile,outfile
  character*(Nchar_mx) resfile
  integer Nlev,Nlay
  double precision pressure(1:Nlev_mx)
  double precision Tsurf
  double precision temperature(1:Nlev_mx)
  double precision height(1:Nlev_mx)
  double precision q_h2o(1:Nlay_mx)
  double precision x_h2o_nominal(1:Nlay_mx)
  double precision x_h2o_rest(1:Nlay_mx)
  double precision x_h2o_water(1:Nlay_mx,1:Nw_mx)
  double precision x_h2o(1:Nlay_mx)
  double precision lw_emissivity,sw_emissivity
  double precision ka_lw_nominal(1:Ngpoints_lw_mx,1:Nlev_mx)
  double precision ka_lw_rest(1:Ngpoints_lw_mx,1:Nlev_mx)
  double precision ka_lw_water(1:Ngpoints_lw_mx,1:Nlev_mx,1:Nw_mx)
  double precision ka_lw(1:Ngpoints_lw_mx,1:Nlev_mx)
  double precision ka_sw_nominal(1:Ngpoints_sw_mx,1:Nlev_mx)
  double precision ka_sw_rest(1:Ngpoints_sw_mx,1:Nlev_mx)
  double precision ka_sw_water(1:Ngpoints_sw_mx,1:Nlev_mx,1:Nw_mx)
  double precision ka_sw(1:Ngpoints_sw_mx,1:Nlev_mx)
  double precision ks_sw_nominal(1:Ngpoints_sw_mx,1:Nlev_mx)
  double precision ks_sw_rest(1:Ngpoints_sw_mx,1:Nlev_mx)
  double precision ks_sw_water(1:Ngpoints_sw_mx,1:Nlev_mx,1:Nw_mx)
  double precision ks_sw(1:Ngpoints_sw_mx,1:Nlev_mx)
  integer Nb_lw
  double precision nu_lo_lw(1:16)
  double precision nu_hi_lw(1:16)
  integer Ng_lw(1:16)
  double precision w_lw_rrtm(1:16,1:20)
  integer Nb_sw
  double precision nu_lo_sw(1:14)
  double precision nu_hi_sw(1:14)
  integer Ng_sw(1:14)
  double precision w_sw_rrtm(1:14,1:20)
  ! temp
  integer ilev,ilay,iband,ib,iq,ig,band,quad,ios
  double precision ka_moy_nominal(1:Nb_lw_mx,1:Nlay_mx)
  double precision ka_moy(1:Nb_lw_mx,1:Nlay_mx)
  logical is_nan
  logical keep_going
  logical infile_ex,outfile_ex
  integer wint
  logical wint_found
  ! parameters
  double precision epsilon
  parameter(epsilon=1.0D-2)
  !
  character*(Nchar_mx) label
  label='program make_optprop'

  !--------------------------------------------------------------------
  ! BEGINNING OF USER INPUT
  !--------------------------------------------------------------------
  ! directories
  path_to_original='../test/i3rc'
  ! files
  original_file='i3rc_mls_cumulus.nc' ! including clouds
  !--------------------------------------------------------------------
  ! END OF USER INPUT
  !--------------------------------------------------------------------
  
  path_to_modified='./ecrad_input'
  path_to_output='./ecrad_output'
  nominal_file='clearsky.nc' ! clear-sky only
  nowater_file='clearsky_nowater.nc' ! clear-sky, no water

  ! Nw, q_min and q_max are read from "data.in"
  infile='./data.in'
  open(10,file=trim(infile),status='old',iostat=ios)
  if (ios.ne.0) then
    call error(label)
    write(*,*) 'File not found:'
    write(*,*) trim(infile)
    stop
  else
    do i=1,3
       read(10,*)
    enddo ! i
    read(10,*) Nw
    read(10,*)
    read(10,*) q_min
    read(10,*)
    read(10,*) q_max
  endif
  close(10)

  inquire(file=trim(path_to_original), exist=dir_ex)
  if (.not.dir_ex) then
     call error(label)
     write(*,*) 'Directory does not exist: ',trim(path_to_original)
     stop
  endif
  call add_slash_if_absent(path_to_original)
  file1=trim(path_to_original)//trim(original_file)
  inquire(file=trim(file1), exist=dir_ex)
  if (.not.dir_ex) then
     call error(label)
     write(*,*) 'File does not exist: ',trim(file1)
     stop
  endif
  inquire(file=trim(path_to_modified), exist=dir_ex)
  if (.not.dir_ex) then
     call error(label)
     write(*,*) 'Directory does not exist: ',trim(path_to_modified)
     stop
  endif
  call add_slash_if_absent(path_to_modified)
  inquire(file=trim(path_to_output), exist=dir_ex)
  if (.not.dir_ex) then
     command='mkdir '//trim(path_to_output)
     call exec(command)
  endif
  call add_slash_if_absent(path_to_output)

  ! ======================================================================
  ! NOMINAL WATER
  ! ======================================================================
  
  modified_file=trim(nominal_file)
  file2=trim(path_to_modified)//trim(modified_file)

  command='cp '//trim(file1)//' '//trim(file2)
  call exec(command)
 
  ! Access path to the file that must be examined
  in_file_name=trim(file2)
  ! Number of variables that must be reset
  Nvar=8
  ! name of each variable, and value it must be reset to
  var_list(1)="overlap_param"
  values(1)=0.0
  var_list(2)="cloud_fraction"
  values(2)=0.0
  var_list(3)="q_ice"
  values(3)=0.0
  var_list(4)="q_liquid"
  values(4)=0.0
  var_list(5)="re_ice"
  values(5)=0.0
  var_list(6)="re_liquid"
  values(6)=0.0
  var_list(7)="inv_cloud_effective_size"
  values(7)=0.0
  var_list(8)="fractional_std"
  values(8)=0.0

  ! Replace values
  istatus = nf90_open(in_file_name, nf90_Write, ncid)
  if (istatus.ne.NF90_NOERR) then
     write(*,*) 'Problem opening file: ',trim(in_file_name)
  else
!     write(*,*) 'File was opened: ',trim(in_file_name)
  endif

  do ivar=1,Nvar
     var_name=var_list(ivar)
     istatus=nf90_inq_varid(ncid,var_name,varid)
     if (istatus.ne.NF90_NOERR) then
        write(*,*) 'Variable ',trim(var_name),' could not be found'
     endif
     istatus = nf90_inquire_variable(ncid, varid, dimids = dimIDs)
     if (istatus.ne.NF90_NOERR) then
        write(*,*) 'Variable ',trim(var_name),' could not be inquired'
     endif
     istatus = nf90_inquire_dimension(ncid, dimIDs(1), len = Nlevels)
     if (istatus.ne.NF90_NOERR) then
        write(*,*) 'Dimension 1 of variable ',trim(var_name),' could not be inquired'
     endif
     istatus = nf90_inquire_dimension(ncid, dimIDs(2), len = Nc)
     if (istatus.ne.NF90_NOERR) then
        write(*,*) 'Dimension 2 of variable ',trim(var_name),' could not be inquired'
     endif
     istatus = nf90_put_var(ncid, varid, reshape((/ (values(ivar), i = 1, Nlevels * Nc) /) , shape = (/ Nlevels, Nc /) ))
     if (istatus.ne.NF90_NOERR) then
        write(*,*) 'Data could not be written for variable ',trim(var_name)
     endif
!     write(*,*) 'variable "',trim(var_name),'" was set to ',values(ivar)
  enddo ! ivar

  istatus=nf90_close(ncid)
  if (istatus.ne.NF90_NOERR) then
     write(*,*) 'Problem closing the file'
  endif
  
  ! ======================================================================
  ! NO WATER
  ! ======================================================================
  
  modified_file=trim(nowater_file)
  file2=trim(path_to_modified)//trim(modified_file)

  command='cp '//trim(file1)//' '//trim(file2)
  call exec(command)
 
  ! Access path to the file that must be examined
  in_file_name=trim(file2)
  ! Number of variables that must be reset
  Nvar=9
  ! name of each variable, and value it must be reset to
  var_list(1)="overlap_param"
  values(1)=0.0
  var_list(2)="cloud_fraction"
  values(2)=0.0
  var_list(3)="q"
  values(3)=0.0
  var_list(4)="q_ice"
  values(4)=0.0
  var_list(5)="q_liquid"
  values(5)=0.0
  var_list(6)="re_ice"
  values(6)=0.0
  var_list(7)="re_liquid"
  values(7)=0.0
  var_list(8)="inv_cloud_effective_size"
  values(8)=0.0
  var_list(9)="fractional_std"
  values(9)=0.0

  ! Replace values
  istatus = nf90_open(in_file_name, nf90_Write, ncid)
  if (istatus.ne.NF90_NOERR) then
     write(*,*) 'Problem opening file: ',trim(in_file_name)
  else
!     write(*,*) 'File was opened: ',trim(in_file_name)
  endif

  do ivar=1,Nvar
     var_name=var_list(ivar)
     istatus=nf90_inq_varid(ncid,var_name,varid)
     if (istatus.ne.NF90_NOERR) then
        write(*,*) 'Variable ',trim(var_name),' could not be found'
     endif
     istatus = nf90_inquire_variable(ncid, varid, dimids = dimIDs)
     if (istatus.ne.NF90_NOERR) then
        write(*,*) 'Variable ',trim(var_name),' could not be inquired'
     endif
     istatus = nf90_inquire_dimension(ncid, dimIDs(1), len = Nlevels)
     if (istatus.ne.NF90_NOERR) then
        write(*,*) 'Dimension 1 of variable ',trim(var_name),' could not be inquired'
     endif
     istatus = nf90_inquire_dimension(ncid, dimIDs(2), len = Nc)
     if (istatus.ne.NF90_NOERR) then
        write(*,*) 'Dimension 2 of variable ',trim(var_name),' could not be inquired'
     endif
     istatus = nf90_put_var(ncid, varid, reshape((/ (values(ivar), i = 1, Nlevels * Nc) /) , shape = (/ Nlevels, Nc /) ))
     if (istatus.ne.NF90_NOERR) then
        write(*,*) 'Data could not be written for variable ',trim(var_name)
     endif
!     write(*,*) 'variable "',trim(var_name),'" was set to ',values(ivar)
  enddo ! ivar

  istatus=nf90_close(ncid)
  if (istatus.ne.NF90_NOERR) then
     write(*,*) 'Problem closing the file'
  endif
  
  ! ======================================================================
  ! WATER
  ! ======================================================================
  write(*,*) 'Generating ECRAD input files in directory:'
  write(*,*) trim(path_to_modified)

  if (Nw.lt.2) then
     call error(label)
     write(*,*) 'Nw=',Nw
     write(*,*) 'should be > 1'
     stop
  endif
  if (Nw.gt.Nw_mx) then
     call error(label)
     write(*,*) 'Nw=',Nw
     write(*,*) '> Nw_mx=',Nw_mx
     stop
  endif
  exp_min=dlog(q_min)
  exp_max=dlog(q_max)
  command='rm -rf ./ecrad_input/water*.nc'
  call exec(command)
  do iw=1,Nw
     exp_iw=exp_min+(exp_max-exp_min)/dble(Nw-1)*dble(iw-1)
     q=exp(exp_iw)
!     write(*,*) 'iw=',iw,' q=',q
     !     q=q_min+(q_max-q_min)/dble(Nw-1)*dble(iw-1)
     call num2str3(iw,str3)
     modified_file='water'&
          //trim(str3)&
          //'.nc'
     file2=trim(path_to_modified)//trim(modified_file)

     command='cp '//trim(file1)//' '//trim(file2)
     call exec(command)

     ! Access path to the file that must be examined
     in_file_name=trim(file2)
     ! Number of variables that must be reset
     Nvar=9
     ! name of each variable, and value it must be reset to
     var_list(1)="overlap_param"
     values(1)=0.0
     var_list(2)="cloud_fraction"
     values(2)=0.0
     var_list(3)="q"
     values(3)=q
     var_list(4)="q_ice"
     values(4)=0.0
     var_list(5)="q_liquid"
     values(5)=0.0
     var_list(6)="re_ice"
     values(6)=0.0
     var_list(7)="re_liquid"
     values(7)=0.0
     var_list(8)="inv_cloud_effective_size"
     values(8)=0.0
     var_list(9)="fractional_std"
     values(9)=0.0

     ! Replace values
     istatus = nf90_open(in_file_name, nf90_Write, ncid)
     if (istatus.ne.NF90_NOERR) then
        write(*,*) 'Problem opening file: ',trim(in_file_name)
     else
!        write(*,*) 'File was opened: ',trim(in_file_name)
     endif

     do ivar=1,Nvar
        var_name=var_list(ivar)
        istatus=nf90_inq_varid(ncid,var_name,varid)
        if (istatus.ne.NF90_NOERR) then
           write(*,*) 'Variable ',trim(var_name),' could not be found'
        endif
        istatus = nf90_inquire_variable(ncid, varid, dimids = dimIDs)
        if (istatus.ne.NF90_NOERR) then
           write(*,*) 'Variable ',trim(var_name),' could not be inquired'
        endif
        istatus = nf90_inquire_dimension(ncid, dimIDs(1), len = Nlevels)
        if (istatus.ne.NF90_NOERR) then
           write(*,*) 'Dimension 1 of variable ',trim(var_name),' could not be inquired'
        endif
        istatus = nf90_inquire_dimension(ncid, dimIDs(2), len = Nc)
        if (istatus.ne.NF90_NOERR) then
           write(*,*) 'Dimension 2 of variable ',trim(var_name),' could not be inquired'
        endif
        istatus = nf90_put_var(ncid, varid, reshape((/ (values(ivar), i = 1, Nlevels * Nc) /) , shape = (/ Nlevels, Nc /) ))
        if (istatus.ne.NF90_NOERR) then
           write(*,*) 'Data could not be written for variable ',trim(var_name)
        endif
!        write(*,*) 'variable "',trim(var_name),'" was set to ',values(ivar)
     enddo ! ivar

     istatus=nf90_close(ncid)
     if (istatus.ne.NF90_NOERR) then
        write(*,*) 'Problem closing the file'
     endif
     
     enddo ! iw
  

  ! ======================================================================
  
666 continue

  infile='./run_ecrad.bash'
  inquire(file=trim(infile), exist=dir_ex)
  if (.not.dir_ex) then
     call error(label)
     write(*,*) 'File does not exist: ',trim(infile)
     stop
  else
    write(*,*) 'Now running ECRAD...'
    command='./run_ecrad.bash > ecrad_output.log 2>&1'
    call exec(command)
  endif

  write(*,*) 'Reading ECRAD optical properties from directory:'
  write(*,*) trim(path_to_output)

  call quadrature_weights(Nb_lw,nu_lo_lw,nu_hi_lw,Ng_lw,w_lw_rrtm,Nb_sw,nu_lo_sw,nu_hi_sw,Ng_sw,w_sw_rrtm)

  infile=trim(path_to_modified)//trim(nominal_file)
  outfile=trim(path_to_output)//'radiative_properties_nominal.nc'
  call read_nc_file(infile,outfile,Nlev,Nlay,pressure,Tsurf,temperature,height, &
       q_h2o,x_h2o_nominal,lw_emissivity,sw_emissivity,ka_lw_nominal,ka_sw_nominal,ks_sw_nominal)
       
  infile=trim(path_to_modified)//trim(nowater_file)
  outfile=trim(path_to_output)//'radiative_properties_nowater.nc'
  call read_nc_file(infile,outfile,Nlev,Nlay,pressure,Tsurf,temperature,height, &
       q_h2o,x_h2o_rest,lw_emissivity,sw_emissivity,ka_lw_rest,ka_sw_rest,ks_sw_rest)

  iw=1
  keep_going=.true.
  do while (keep_going)
     call num2str3(iw,str3)
     infile=trim(path_to_modified)&
          //'water'&
          //trim(str3)&
          //'.nc'
     inquire(file=trim(infile),exist=infile_ex)
!     write(*,*) 'infile=',trim(infile),' exist=',infile_ex
     outfile=trim(path_to_output)&
          //'radiative_properties_water'&
          //trim(str3)&
          //'.nc'
     inquire(file=trim(outfile),exist=outfile_ex)
!     write(*,*) 'infile=',trim(outfile),' exist=',outfile_ex
     if ((infile_ex).and.(outfile_ex)) then
        Nw=iw
        if (Nw.gt.Nw_mx) then
           call error(label)
           write(*,*) 'Nw=',Nw
           write(*,*) '> Nw_mx=',Nw_mx
           stop
        endif
        call read_nc_file(infile,outfile,Nlev,Nlay,pressure,Tsurf,temperature,height, &
             q_h2o,x_h2o,lw_emissivity,sw_emissivity,ka_lw,ka_sw,ks_sw)
        
        do ilay=1,Nlay
           x_h2o_water(ilay,iw)=x_h2o(ilay)
           ig=0
           do ib=1,Nb_lw
              do iq=1,Ng_lw(ib)
                 ig=ig+1
                 ka_lw_water(ig,ilay,iw)=ka_lw(ig,ilay)
              enddo ! iq
           enddo ! ib
           ig=0
           do ib=1,Nb_sw
              do iq=1,Ng_sw(ib)
                 ig=ig+1
                 ka_sw_water(ig,ilay,iw)=ka_sw(ig,ilay)
                 ks_sw_water(ig,ilay,iw)=ks_sw(ig,ilay)
              enddo ! iq
           enddo ! ib
        enddo ! ilay
!        write(*,*) 'iw=',iw,' x_h2o_water=',x_h2o_water(1,iw)
        iw=iw+1
     else
        keep_going=.false.
     endif
  enddo ! while (keep_going)

  do i=1,Nlay
        wint_found=.false.
        do iw=1,Nw-1
           if ((x_h2o_nominal(i).ge.x_h2o_water(i,iw)).and.(x_h2o_nominal(i).le.x_h2o_water(i,iw+1))) then
              wint_found=.true.
              wint=iw
              goto 531
           endif
        enddo      ! iw
531     continue
        if (.not.wint_found) then
           if (x_h2o_nominal(i).lt.x_h2o_water(i,1)) then
              wint_found=.true.
              wint=0
           endif
           if (x_h2o_nominal(i).gt.x_h2o_water(i,Nw)) then
              call error(label)
              write(*,*) 'x_h2o_nominal(',i,')=',x_h2o_nominal(i)
              write(*,*) '> x_h2o_water(',i,',',Nw,')=',x_h2o_water(i,Nw)
              write(*,*) 'make new tabulation'
              stop
           endif
        endif
        if (.not.wint_found) then
           call error(label)
           write(*,*) 'water interval not found'
           stop
        endif
        if (x_h2o_nominal(i).lt.x_h2o_water(i,wint)) then
           call error(label)
           write(*,*) 'x_h2o_nominal(',i,')=',x_h2o_nominal(i)
           write(*,*) '< x_h2o_water(',i,',',wint,')=',x_h2o_water(i,wint)
           stop
        endif
        if (x_h2o_nominal(i).gt.x_h2o_water(i,wint+1)) then
           call error(label)
           write(*,*) 'x_h2o_nominal(',i,')=',x_h2o_nominal(i)
           write(*,*) '> x_h2o_water(',i,',',wint+1,')=',x_h2o_water(i,wint+1)
           stop
        endif

!        LW        
!        ig=0
!        do ib=1,Nb_lw
!           do iq=1,Ng_lw(ib)
!              ig=ig+1
!              if (ka_lw_nominal(ig,i).lt.ka_lw_water(ig,i,iw)*(1.0D+0-epsilon)) then
!                 call error(label)
!                 write(*,*) 'ka_lw_nominal(',ig,',',i,')=',ka_lw_nominal(ig,i)
!                 write(*,*) '< ka_lw_water(',ig,',',i,',',iw,')=',ka_lw_water(ig,i,iw)
!                 !                 stop
!              endif
!              if (ka_lw_nominal(ig,i).gt.ka_lw_water(ig,i,iw+1)*(1.0D+0+epsilon)) then
!                 call error(label)
!                 write(*,*) 'ka_lw_nominal(',ig,',',i,')=',ka_lw_nominal(ig,i)
!                 write(*,*) '> ka_lw_water(',ig,',',i,',',iw+1,')=',ka_lw_water(ig,i,iw+1)
!                 !                 stop
!              endif
!           enddo ! iq
!        enddo ! ib
!        SW        
!        ig=0
!        do ib=1,Nb_sw
!           do iq=1,Ng_sw(ib)
!              ig=ig+1
!              if (ka_sw_nominal(ig,i).lt.ka_sw_water(ig,i,iw)*(1.0D+0-epsilon)) then
!                 call error(label)
!                 write(*,*) 'ka_sw_nominal(',ig,',',i,')=',ka_sw_nominal(ig,i)
!                 write(*,*) '< ka_sw_water(',ig,',',i,',',iw,')=',ka_sw_water(ig,i,iw)
!!                 stop
!              endif
!              if (ka_sw_nominal(ig,i).gt.ka_sw_water(ig,i,iw+1)*(1.0D+0+epsilon)) then
!                 call error(label)
!                 write(*,*) 'ka_sw_nominal(',ig,',',i,')=',ka_sw_nominal(ig,i)
!                 write(*,*) '> ka_sw_water(',ig,',',i,',',iw+1,')=',ka_sw_water(ig,i,iw+1)
!!                stop
!              endif
!              if (ks_sw_nominal(ig,i).lt.ks_sw_water(ig,i,iw)*(1.0D+0-epsilon)) then
!                 call error(label)
!                 write(*,*) 'ks_sw_nominal(',ig,',',i,')=',ks_sw_nominal(ig,i)
!                 write(*,*) '< ks_sw_water(',ig,',',i,',',iw,')=',ks_sw_water(ig,i,iw)
!!                 stop
!              endif
!              if (ks_sw_nominal(ig,i).gt.ks_sw_water(ig,i,iw+1)*(1.0D+0+epsilon)) then
!                 call error(label)
!                 write(*,*) 'ks_sw_nominal(',ig,',',i,')=',ks_sw_nominal(ig,i)
!                 write(*,*) '> ks_sw_water(',ig,',',i,',',iw+1,')=',ks_sw_water(ig,i,iw+1)
!!                 stop
!              endif
!           enddo ! iq
!        enddo ! ib
     enddo ! i
!  ! Tests

  resfile='./ecrad_opt_prop.txt'
  call generate_opt_prop_file(resfile,Nw,Nlev,Nlay,Tsurf,pressure, &
    temperature,height,x_h2o_nominal,x_h2o_water, &
    lw_emissivity,sw_emissivity, &
    Nb_lw,nu_lo_lw,nu_hi_lw,Ng_lw,w_lw_rrtm, &
    Nb_sw,nu_lo_sw,nu_hi_sw,Ng_sw,w_sw_rrtm, &
    ka_lw_nominal,ka_lw_water, &
    ka_sw_nominal,ka_sw_water, &
    ks_sw_nominal,ks_sw_water)
  
end program make_optprop
