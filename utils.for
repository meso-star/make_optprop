c     Copyright (C) 2008-2009 Vincent Eymet
c
c     Planet_EMC is free software; you can redistribute it and/or modify
c     it under the terms of the GNU General Public License as published by
c     the Free Software Foundation; either version 3, or (at your option)
c     any later version.

c     Planet_EMC is distributed in the hope that it will be useful,
c     but WITHOUT ANY WARRANTY; without even the implied warranty of
c     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
c     GNU General Public License for more details.

c     You should have received a copy of the GNU General Public License
c     along with Planet_EMC; if not, see <http://www.gnu.org/licenses/>
c
      subroutine logfile_name(pindex,event,logfile)
      implicit none
      include 'max.inc'
c
c     Purpose: to produce logfile name based on process and event indexes
c
c     Inputs:
c       + pindex: process index
c       + event: event index
c
c     Output:
c       + logfile: logfile name
c

c     input
      integer pindex,event
c     output
      character*(Nchar_mx) logfile
c     temp
      character*3 pch
      character*10 ech
      integer strlen
      character*(Nchar_mx) label
      label='subroutine logfile_name'
      
      call num2str3(pindex,pch)
      call num2str(event,ech)

      logfile='./logs/error_process'//
     &     pch(1:strlen(pch))//
     &     '_event'//
     &     ech(1:strlen(ech))//
     &     '.log'

      return
      end



      subroutine num2str3(num,str3)
      implicit none
      include 'max.inc'
      include 'formats.inc'
c
c     Purpose: to convert an integer to a character string of size 3
c
c     Input:
c       + num: integer value (must be positive and less than 1000)
c
c     Output:
c       + str3: character string of size 3
c
c     I/O
      integer num
      character*3 str3
c     temp
      character*1 zeroch,kch1
      character*2 kch2
c     label
      integer strlen
      character*(Nchar_mx) label
      label='subroutine num2str3'
      
      write(zeroch,11) 0
      if ((num.ge.0).and.(num.lt.10)) then
         write(kch1,11) num
         str3=zeroch(1:strlen(zeroch))
     &        //zeroch(1:strlen(zeroch))
     &        //kch1(1:strlen(kch1))
      else if ((num.ge.10).and.(num.lt.100)) then
         write(kch2,12) num
         str3=zeroch(1:strlen(zeroch))
     &        //kch2(1:strlen(kch2))
      else if ((num.ge.100).and.(num.lt.1000)) then
         write(str3,13) num
      else
         call error(label)
         write(*,*) 'num=',num,' >= 1000'
         stop
      endif
      
      return
      end



      subroutine num2str(num,str)
      implicit none
      include 'max.inc'
      include 'formats.inc'
c
c     Purpose: to convert a positive integer to a character string
c     Warning: maximum string size is 10 characters, and maximum
c     value for "num" is 2147483647
c
c     Input:
c       + num: integer value
c     
c     Output:
c       + str: character string of size 10
c
c     I/O
      integer num
      character*10 str
c     temp
      character*1 s1
      character*2 s2
      character*3 s3
      character*4 s4
      character*5 s5
      character*6 s6
      character*7 s7
      character*8 s8
      character*9 s9
      character*10 s10
c     label
      integer strlen
      character*(Nchar_mx) label
      label='subroutine num2str'

      if ((num.ge.0).and.(num.lt.10)) then
         write(s1,11) num
         str=s1
      else if ((num.ge.10).and.(num.lt.100)) then
         write(s2,12) num
         str=s2
      else if ((num.ge.100).and.(num.lt.1000)) then
         write(s3,13) num
         str=s3
      else if ((num.ge.1000).and.(num.lt.10000)) then
         write(s4,14) num
         str=s4
      else if ((num.ge.10000).and.(num.lt.100000)) then
         write(s5,15) num
         str=s5
      else if ((num.ge.100000).and.(num.lt.1000000)) then
         write(s6,16) num
         str=s6
      else if ((num.ge.1000000).and.(num.lt.10000000)) then
         write(s7,17) num
         str=s7
      else if ((num.ge.10000000).and.(num.lt.100000000)) then
         write(s8,18) num
         str=s8
      else if ((num.ge.100000000).and.(num.lt.1000000000)) then
         write(s9,19) num
         str=s9
      else if ((num.ge.1000000000).and.(num.le.2147483647)) then
         write(s10,110) num
         str=s10
      else
         call error(label)
         write(*,*) 'Bad input argument:'
         write(*,*) 'num=',num
         stop
      endif

      return
      end



      subroutine str2num(str,num)
      implicit none
      include 'max.inc'
      include 'formats.inc'
c
c     Purpose: to convert a character string (that is composed of numeric characters only)
c     into its numeric value
c
c     Input:
c       + str: character string, composed of numeric characters (maximum 10)
c
c     Output:
c       + num: numeric integer value
c

c     input
      character*10 str
c     output
      integer num
c     temp
      integer n,integ,integf
      integer i,j
      character*1 num_ch(0:9),ch
c     label
      integer strlen
      character*(Nchar_mx) label
      label='subroutine str2num'

      num_ch(0)='0'
      num_ch(1)='1'
      num_ch(2)='2'
      num_ch(3)='3'
      num_ch(4)='4'
      num_ch(5)='5'
      num_ch(6)='6'
      num_ch(7)='7'
      num_ch(8)='8'
      num_ch(9)='9'

      n=strlen(str)
      num=0
      do i=1,n
         ch=str(i:i+1)
c     Debug
c         write(*,*) 'i=',i,' ch="',ch(1:strlen(ch)),'"'
c     Debug
         integf=0
         do j=0,9
            if (ch(1:strlen(ch)).eq.num_ch(j)) then
               integf=1
               integ=j
               goto 111
            endif
         enddo ! j
 111     continue
         if (integf.eq.0) then
            call error(label)
            write(*,*) 'character index',i
            write(*,*) 'from string:',str(1:n)
            write(*,*) 'that is:"',ch(1:strlen(ch)),'"'
            write(*,*) 'was not identified as numeric'
            stop
         else
c     Debug
c            write(*,*) 'integ=',integ
c     Debug
            num=num+int(integ*10.0D+0**(n-i))
c     Debug
c            write(*,*) 'num=',num
c     Debug
         endif
      enddo ! i
      
      return
      end


      function frac(x)
      implicit none
      include 'max.inc'
c
c     Purpose: to compute the fractional part of a double precision value
c
      double precision frac,x
      
      frac=x-int(x)
      
      return
      end

      
      subroutine z2r(Radius,z,r)
      implicit none
      include 'max.inc'
c     
c     Purpose: to convert an altitude into a radius
c     
c     Inputs:
c       + Radius: radius of the reference level (m)
c       + z: altitude (m)
c
c     Outputs:
c       + r: radius (m)
c
c     I/O
      double precision Radius,z
      double precision r
c     temp
      integer strlen
      character*(Nchar_mx) label
      label='subroutine z2r'

      r=Radius+z

      return
      end


      subroutine nu2lambda(nu,lambda)
      implicit none
      include 'max.inc'
c
c     Purpose: to convert a wavenumber (cm��) into a wavelength (�m)
c
c     I/O
      double precision nu,lambda
c     temp
      integer strlen
      character*(Nchar_mx) label
      label='subroutine nu2lambda'

      if (nu.le.0.0D+0) then
         call error(label)
         write(*,*) 'Bad input parameter:'
         write(*,*) 'nu=',nu
         stop
      else
         lambda=1.0D+4/nu
      endif

      return
      end



      subroutine lambda2nu(lambda,nu)
      implicit none
      include 'max.inc'
c
c     Purpose: to convert a wavelength (�m) into a wavenumber (cm��)
c
c     I/O
      double precision lambda,nu
c     temp
      integer strlen
      character*(Nchar_mx) label
      label='subroutine lambda2nu'

      if (lambda.le.0.0D+0) then
         call error(label)
         write(*,*) 'Bad input parameter:'
         write(*,*) 'lambda=',lambda
         stop
      else
         nu=1.0D+4/lambda
      endif

      return
      end
