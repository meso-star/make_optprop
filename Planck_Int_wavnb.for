      real function PLKAVG(WNUMLO,WNUMHI,T)

c        Computes Planck function integrated between two wavenumbers

c  NOTE ** Change R1MACH to D1MACH to run in double precision

c  INPUT :  WNUMLO : Lower wavenumber (inv cm) of spectral interval
c           WNUMHI : Upper wavenumber
c           T      : Temperature (K)

c  OUTPUT : PLKAVG : Integrated Planck function ( Watts/sq m )
c                      = Integral (WNUMLO to WNUMHI) of
c                        2h c**2  nu**3 / ( EXP(hc nu/kT) - 1)
c                        (where h=Plancks constant, c=speed of
c                         light, nu=wavenumber, T=temperature,
c                         and k = Boltzmann constant)

c  Reference : Specifications of the Physical World: New Value
c                 of the Fundamental Constants, Dimensions/N.B.S.,
c                 Jan. 1974

c  Method :  For WNUMLO close to WNUMHI, a Simpson-rule quadrature
c            is done to avoid ill-conditioning; otherwise

c            (1)  For WNUMLO or WNUMHI small,
c                 integral(0 to WNUMLO/HI) is calculated by expanding
c                 the integrand in a power series and integrating
c                 term by term;

c            (2)  Otherwise, integral(WNUMLO/HI to INFINITY) is
c                 calculated by expanding the denominator of the
c                 integrand in powers of the exponential and
c                 integrating term by term.

c  Accuracy :  At least 6 significant digits, assuming the
c              physical constants are infinitely accurate

c  ERRORS WHICH ARE NOT TRAPPED:

c      * power or exponential series may underflow, giving no
c        significant digits.  This may or may not be of concern,
c        depending on the application.

c      * Simpson-rule special case is skipped when denominator of
c        integrand will cause overflow.  In that case the normal
c        procedure is used, which may be inaccurate if the
c        wavenumber limits (WNUMLO, WNUMHI) are close together.

c  LOCAL VARIABLES

c        A1,2,... :  Power series coefficients
c        C2       :  h * c / k, in units cm*K (h = Plancks constant,
c                      c = speed of light, k = Boltzmann constant)
c        D(I)     :  Exponential series expansion of integral of
c                       Planck function from WNUMLO (i=1) or WNUMHI
c                       (i=2) to infinity
c        EPSIL    :  SMALLEST NUMBER SUCH THAT 1+EPSIL .gt. 1 on
c                       computer
c        EX       :  EXP( - V(I) )
c        EXM      :  EX**M
c        MMAX     :  No. of terms to take in exponential series
c        MV       :  Multiples of V(I)
c        P(I)     :  Power series expansion of integral of
c                       Planck function from zero to WNUMLO (I=1) or
c                       WNUMHI (I=2)
c        PI       :  3.14159...
c        SIGMA    :  Stefan-Boltzmann constant (W/m**2/K**4)
c        SIGDPI   :  SIGMA / PI
c        SMALLV   :  Number of times the power series is used (0,1,2)
c        V(I)     :  C2 * (WNUMLO(I=1) or WNUMHI(I=2)) / temperature
c        VCUT     :  Power-series cutoff point
c        VCP      :  Exponential series cutoff points
c        VMAX     :  Largest allowable argument of EXP function

c   Called by- DISORT
c   Calls- R1MACH, ERRMSG
c ----------------------------------------------------------------------

c     .. Parameters ..

      real      A1, A2, A3, A4, A5, A6
      parameter ( A1 = 1. / 3., A2 = -1. / 8., A3 = 1. / 60.,
     &          A4 = -1. / 5040., A5 = 1. / 272160.,
     &          A6 = -1. / 13305600. )
c     ..
c     .. Scalar Arguments ..

      real      T, WNUMHI, WNUMLO
c     ..
c     .. Local Scalars ..

      integer   I, K, M, MMAX, N, SMALLV
      real      C2, CONC, DEL, EPSIL, EX, EXM, HH, MV, OLDVAL, PI,
     &          SIGDPI, SIGMA, VAL, VAL0, VCUT, VMAX, VSQ, X
c     ..
c     .. Local Arrays ..

      real      D( 2 ), P( 2 ), V( 2 ), VCP( 7 )
c     ..
c     .. External Functions ..

      real      R1MACH
      external  R1MACH
c     ..
c     .. External Subroutines ..

c      external  ERRMSG
c     ..
c     .. Intrinsic Functions ..

      intrinsic ABS, ASIN, EXP, LOG, MOD
c     ..
c     .. Statement Functions ..

      real      PLKF
c     ..
      save      PI, CONC, VMAX, EPSIL, SIGDPI

      data      C2 / 1.438786 / , SIGMA / 5.67032E-8 / , VCUT / 1.5 / ,
     &          VCP / 10.25, 5.7, 3.9, 2.9, 2.3, 1.9, 0.0 /
      data      PI / 0.0 /

c     .. Statement Function definitions ..

c     ..


      if (PI.eq.0.0) then
         PI     = 2.*ASIN( 1.0 )
         VMAX   = LOG( R1MACH( 2 ) )
         EPSIL  = R1MACH( 4 )
         SIGDPI = SIGMA / PI
         CONC   = 15. / PI**4
      endif

      if ((T.lt.0.0).or.(WNUMHI.lt.WNUMLO).or.(WNUMLO.lt.0.)) then
         write(*,*) 'PLKAVG--temperature or wavenums. wrong'
         write(*,*) 'T=',T
         write(*,*) 'wnumhi=',wnumhi,' wnumlo=',wnumlo
         stop
      endif
      
      if (T.lt.1.E-4) then
c     Debug
c         write(*,*) 'T=',T
c     Debug
         PLKAVG = 0.0
         return
      endif


      V( 1 ) = C2*WNUMLO / T
      V( 2 ) = C2*WNUMHI / T
c                             ** Special case: close wavenumbers

      if ((V(1).gt.EPSIL).and.(V(2).lt.VMAX).and.
     &    ((WNUMHI-WNUMLO)/WNUMHI.lt.1.E-2)) then

c                          ** Wavenumbers are very close.  Get integral
c                             by iterating Simpson rule to convergence.

         HH     = V( 2 ) - V( 1 )
         OLDVAL = 0.0
         VAL0   = PLKF( V( 1 ) ) + PLKF( V( 2 ) )

         do 20 N=1,10
            DEL  = HH / ( 2*N )
            VAL  = VAL0
            do 10 K=1,2*N-1
               VAL  = VAL + 2*( 1 + MOD( K,2 ) )*
     &                      PLKF( V( 1 ) + K*DEL )
   10       continue
            VAL  = DEL / 3.*VAL
            if ( ABS( ( VAL - OLDVAL ) / VAL ).LE.1.E-6 ) goto  30
            OLDVAL = VAL
   20    continue
         write(*,*) 'PLKAVG--Simpson rule didn''t converge'
   30    continue
         PLKAVG = SIGDPI * T**4 * CONC * VAL
         return
      endif

      SMALLV = 0
      do 60 I = 1, 2
         if (V(I).lt.VCUT) then
c                                   ** Use power series
            SMALLV = SMALLV + 1
            VSQ    = V( I )**2
            P( I ) = CONC*VSQ*V( I )*( A1 + V( I )*
     &               ( A2 + V( I )*( A3 + VSQ*( A4 + 
     &               VSQ*( A5 + VSQ*A6 ) ) ) ) )
         else
c                      ** Use exponential series
            MMAX  = 0
c                                ** Find upper limit of series
   40       continue
            MMAX  = MMAX + 1
            if (V(I).lt.VCP(MMAX)) goto 40
            EX     = EXP( - V( I ) )
            EXM    = 1.0
            D( I ) = 0.0
            do 50 M=1,MMAX
               MV     = M*V( I )
               EXM    = EX*EXM
               D( I ) = D( I ) + EXM*( 6.+ MV*( 6.+ MV*( 3.+ MV ) ) )
     &                  / M**4
   50       continue
            D(I) = CONC*D(I)
         endif
   60 continue

c                              ** Handle ill-conditioning
      if (SMALLV.eq.2) then
c                                    ** WNUMLO and WNUMHI both small
         PLKAVG = P( 2 ) - P( 1 )
      else if (SMALLV.eq.1) then
c                                    ** WNUMLO small, WNUMHI large
         PLKAVG = 1.- P( 1 ) - D( 2 )
      else
c                                    ** WNUMLO and WNUMHI both large
         PLKAVG = D( 1 ) - D( 2 )
      endif

      PLKAVG = SIGDPI * T**4 * PLKAVG

c      if ( PLKAVG.eq.0.0 ) write(*,*)
c     &    'PLKAVG--returns zero; possible underflow'
      end

      real function PLKF(X)
      implicit none
      real X
      PLKF= X**3 / ( EXP( X ) - 1 )
      return
      end


      real function R1MACH (I)
C***BEGIN PROLOGUE  R1MACH
C***PURPOSE  Return floating point machine dependent constants.
C***LIBRARY   SLATEC
C***CATEGORY  R1
C***TYPE      SINGLE PRECISION (R1MACH-S, D1MACH-D)
C***KEYWORDS  MACHINE CONSTANTS
C***AUTHOR  Fox, P. A., (Bell Labs)
C           Hall, A. D., (Bell Labs)
C           Schryer, N. L., (Bell Labs)
C***DESCRIPTION
C
C   R1MACH can be used to obtain machine-dependent parameters for the
C   local machine environment.  It is a function subprogram with one
C   (input) argument, and can be referenced as follows:
C
C        A = R1MACH(I)
C
C   where I=1,...,5.  The (output) value of A above is determined by
C   the (input) value of I.  The results for various values of I are
C   discussed below.
C
C   R1MACH(1) = B**(EMIN-1), the smallest positive magnitude.
C   R1MACH(2) = B**EMAX*(1 - B**(-T)), the largest magnitude.
C   R1MACH(3) = B**(-T), the smallest relative spacing.
C   R1MACH(4) = B**(1-T), the largest relative spacing.
C   R1MACH(5) = LOG10(B)
C
C   Assume single precision numbers are represented in the T-digit,
C   base-B form
C
C              sign (B**E)*( (X(1)/B) + ... + (X(T)/B**T) )
C
C   where 0 .LE. X(I) .lt. B for I=1,...,T, 0 .lt. X(1), and
C   EMIN .LE. E .LE. EMAX.
C
C   The values of B, T, EMIN and EMAX are provided in I1MACH as
C   follows:
C   I1MACH(10) = B, the base.
C   I1MACH(11) = T, the number of base-B digits.
C   I1MACH(12) = EMIN, the smallest exponent E.
C   I1MACH(13) = EMAX, the largest exponent E.
C
C   To alter this function for a particular environment, the desired
C   set of data statements should be activated by removing the C from
C   column 1.  Also, the values of R1MACH(1) - R1MACH(4) should be
C   checked for consistency with the local operating system.
C
C***REFERENCES  P. A. Fox, A. D. Hall and N. L. Schryer, Framework for
C                 a portable library, ACM Transactions on Mathematical
C                 Software 4, 2 (June 1978), pp. 177-188.
C***ROUTINES CALLED  XERMSG
C***REVISION HISTORY  (YYMMDD)
C   790101  DATE WRITTEN
C   890213  REVISION DATE from Version 3.2
C   891214  Prologue converted to Version 4.0 format.  (BAB)
C   900315  CALLs to XERROR changed to CALLs to XERMSG.  (THJ)
C   900618  Added DEC RISC constants.  (WRB)
C   900723  Added IBM RS 6000 constants.  (WRB)
C   910710  Added HP 730 constants.  (SMR)
C   911114  Added Convex IEEE constants.  (WRB)
C   920121  Added SUN -r8 compiler option constants.  (WRB)
C   920229  Added Touchstone Delta i860 constants.  (WRB)
C   920501  Reformatted the REFERENCES section.  (WRB)
C   920625  Added CONVEX -p8 and -pd8 compiler option constants.
C           (BKS, WRB)
C   930201  Added DEC Alpha and SGI constants.  (RWC and WRB)
C***end PROLOGUE  R1MACH
C
      integer SMALL(2)
      integer LARGE(2)
      integer RIGHT(2)
      integer DIVER(2)
      integer LOG10(2)
C
      real RMACH(5)
      save RMACH
C
      equivalence (RMACH(1),SMALL(1))
      equivalence (RMACH(2),LARGE(1))
      equivalence (RMACH(3),RIGHT(1))
      equivalence (RMACH(4),DIVER(1))
      equivalence (RMACH(5),LOG10(1))
C
C     MACHINE CONSTANTS FOR THE AMIGA
C     ABSOFT FORTRAN COMPILER USING THE 68020/68881 COMPILER OPTION
C
C     data SMALL(1) / Z'00800000' /
C     data LARGE(1) / Z'7F7FFFFF' /
C     data RIGHT(1) / Z'33800000' /
C     data DIVER(1) / Z'34000000' /
C     data LOG10(1) / Z'3E9A209B' /
C
C     MACHINE CONSTANTS FOR THE AMIGA
C     ABSOFT FORTRAN COMPILER USING SOFTWARE FLOATING POINT
C
C     data SMALL(1) / Z'00800000' /
C     data LARGE(1) / Z'7EFFFFFF' /
C     data RIGHT(1) / Z'33800000' /
C     data DIVER(1) / Z'34000000' /
C     data LOG10(1) / Z'3E9A209B' /
C
C     MACHINE CONSTANTS FOR THE APOLLO
C
C     data SMALL(1) / 16#00800000 /
C     data LARGE(1) / 16#7FFFFFFF /
C     data RIGHT(1) / 16#33800000 /
C     data DIVER(1) / 16#34000000 /
C     data LOG10(1) / 16#3E9A209B /
C
C     MACHINE CONSTANTS FOR THE BURROUGHS 1700 SYSTEM
C
C     data RMACH(1) / Z400800000 /
C     data RMACH(2) / Z5FFFFFFFF /
C     data RMACH(3) / Z4E9800000 /
C     data RMACH(4) / Z4EA800000 /
C     data RMACH(5) / Z500E730E8 /
C
C     MACHINE CONSTANTS FOR THE BURROUGHS 5700/6700/7700 SYSTEMS
C
C     data RMACH(1) / O1771000000000000 /
C     data RMACH(2) / O0777777777777777 /
C     data RMACH(3) / O1311000000000000 /
C     data RMACH(4) / O1301000000000000 /
C     data RMACH(5) / O1157163034761675 /
C
C     MACHINE CONSTANTS FOR THE CDC 170/180 SERIES USING NOS/VE
C
C     data RMACH(1) / Z"3001800000000000" /
C     data RMACH(2) / Z"4FFEFFFFFFFFFFFE" /
C     data RMACH(3) / Z"3FD2800000000000" /
C     data RMACH(4) / Z"3FD3800000000000" /
C     data RMACH(5) / Z"3FFF9A209A84FBCF" /
C
C     MACHINE CONSTANTS FOR THE CDC 6000/7000 SERIES
C
C     data RMACH(1) / 00564000000000000000B /
C     data RMACH(2) / 37767777777777777776B /
C     data RMACH(3) / 16414000000000000000B /
C     data RMACH(4) / 16424000000000000000B /
C     data RMACH(5) / 17164642023241175720B /
C
C     MACHINE CONSTANTS FOR THE CELERITY C1260
C
C     data SMALL(1) / Z'00800000' /
C     data LARGE(1) / Z'7F7FFFFF' /
C     data RIGHT(1) / Z'33800000' /
C     data DIVER(1) / Z'34000000' /
C     data LOG10(1) / Z'3E9A209B' /
C
C     MACHINE CONSTANTS FOR THE CONVEX
C     USING THE -fn COMPILER OPTION
C
C     data RMACH(1) / Z'00800000' /
C     data RMACH(2) / Z'7FFFFFFF' /
C     data RMACH(3) / Z'34800000' /
C     data RMACH(4) / Z'35000000' /
C     data RMACH(5) / Z'3F9A209B' /
C
C     MACHINE CONSTANTS FOR THE CONVEX
C     USING THE -fi COMPILER OPTION
C
C     data RMACH(1) / Z'00800000' /
C     data RMACH(2) / Z'7F7FFFFF' /
C     data RMACH(3) / Z'33800000' /
C     data RMACH(4) / Z'34000000' /
C     data RMACH(5) / Z'3E9A209B' /
C
C     MACHINE CONSTANTS FOR THE CONVEX
C     USING THE -p8 OR -pd8 COMPILER OPTION
C
C     data RMACH(1) / Z'0010000000000000' /
C     data RMACH(2) / Z'7FFFFFFFFFFFFFFF' /
C     data RMACH(3) / Z'3CC0000000000000' /
C     data RMACH(4) / Z'3CD0000000000000' /
C     data RMACH(5) / Z'3FF34413509F79FF' /
C
C     MACHINE CONSTANTS FOR THE CRAY
C
C     data RMACH(1) / 200034000000000000000B /
C     data RMACH(2) / 577767777777777777776B /
C     data RMACH(3) / 377224000000000000000B /
C     data RMACH(4) / 377234000000000000000B /
C     data RMACH(5) / 377774642023241175720B /
C
C     MACHINE CONSTANTS FOR THE data GENERAL ECLIPSE S/200
C     NOTE - IT MAY BE APPROPRIATE TO INCLUDE THE FOLLOWING CARD -
C     STATIC RMACH(5)
C
C     data SMALL /    20K,       0 /
C     data LARGE / 77777K, 177777K /
C     data RIGHT / 35420K,       0 /
C     data DIVER / 36020K,       0 /
C     data LOG10 / 40423K,  42023K /
C
C     MACHINE CONSTANTS FOR THE DEC ALPHA
C     USING G_FLOAT
C
C     data RMACH(1) / '00000080'X /
C     data RMACH(2) / 'FFFF7FFF'X /
C     data RMACH(3) / '00003480'X /
C     data RMACH(4) / '00003500'X /
C     data RMACH(5) / '209B3F9A'X /
C
C     MACHINE CONSTANTS FOR THE DEC ALPHA
C     USING IEEE_FLOAT
C
C     data RMACH(1) / '00800000'X /
C     data RMACH(2) / '7F7FFFFF'X /
C     data RMACH(3) / '33800000'X /
C     data RMACH(4) / '34000000'X /
C     data RMACH(5) / '3E9A209B'X /
C
C     MACHINE CONSTANTS FOR THE DEC RISC
C
C     data RMACH(1) / Z'00800000' /
C     data RMACH(2) / Z'7F7FFFFF' /
C     data RMACH(3) / Z'33800000' /
C     data RMACH(4) / Z'34000000' /
C     data RMACH(5) / Z'3E9A209B' /
C
C     MACHINE CONSTANTS FOR THE DEC VAX
C     (EXPRESSED IN integer AND HEXADECIMAL)
C     THE HEX FORMAT BELOW MAY NOT BE SUITABLE FOR UNIX SYSTEMS
C     THE integer FORMAT SHOULD BE OK FOR UNIX SYSTEMS
C
C     data SMALL(1) /       128 /
C     data LARGE(1) /    -32769 /
C     data RIGHT(1) /     13440 /
C     data DIVER(1) /     13568 /
C     data LOG10(1) / 547045274 /
C
C     data SMALL(1) / Z00000080 /
C     data LARGE(1) / ZFFFF7FFF /
C     data RIGHT(1) / Z00003480 /
C     data DIVER(1) / Z00003500 /
C     data LOG10(1) / Z209B3F9A /
C
C     MACHINE CONSTANTS FOR THE ELXSI 6400
C     (ASSUMING real*4 IS THE DEFAULT real)
C
C     data SMALL(1) / '00800000'X /
C     data LARGE(1) / '7F7FFFFF'X /
C     data RIGHT(1) / '33800000'X /
C     data DIVER(1) / '34000000'X /
C     data LOG10(1) / '3E9A209B'X /
C
C     MACHINE CONSTANTS FOR THE HARRIS 220
C
C     data SMALL(1), SMALL(2) / '20000000, '00000201 /
C     data LARGE(1), LARGE(2) / '37777777, '00000177 /
C     data RIGHT(1), RIGHT(2) / '20000000, '00000352 /
C     data DIVER(1), DIVER(2) / '20000000, '00000353 /
C     data LOG10(1), LOG10(2) / '23210115, '00000377 /
C
C     MACHINE CONSTANTS FOR THE HONEYWELL 600/6000 SERIES
C
C     data RMACH(1) / O402400000000 /
C     data RMACH(2) / O376777777777 /
C     data RMACH(3) / O714400000000 /
C     data RMACH(4) / O716400000000 /
C     data RMACH(5) / O776464202324 /
C
C     MACHINE CONSTANTS FOR THE HP 730
C
C     data RMACH(1) / Z'00800000' /
C     data RMACH(2) / Z'7F7FFFFF' /
C     data RMACH(3) / Z'33800000' /
C     data RMACH(4) / Z'34000000' /
C     data RMACH(5) / Z'3E9A209B' /
C
C     MACHINE CONSTANTS FOR THE HP 2100
C     3 WORD double precision WITH FTN4
C
C     data SMALL(1), SMALL(2) / 40000B,       1 /
C     data LARGE(1), LARGE(2) / 77777B, 177776B /
C     data RIGHT(1), RIGHT(2) / 40000B,    325B /
C     data DIVER(1), DIVER(2) / 40000B,    327B /
C     data LOG10(1), LOG10(2) / 46420B,  46777B /
C
C     MACHINE CONSTANTS FOR THE HP 2100
C     4 WORD double precision WITH FTN4
C
C     data SMALL(1), SMALL(2) / 40000B,       1 /
C     data LARGE(1), LARGE(2) / 77777B, 177776B /
C     data RIGHT(1), RIGHT(2) / 40000B,    325B /
C     data DIVER(1), DIVER(2) / 40000B,    327B /
C     data LOG10(1), LOG10(2) / 46420B,  46777B /
C
C     MACHINE CONSTANTS FOR THE HP 9000
C
C     data SMALL(1) / 00004000000B /
C     data LARGE(1) / 17677777777B /
C     data RIGHT(1) / 06340000000B /
C     data DIVER(1) / 06400000000B /
C     data LOG10(1) / 07646420233B /
C
C     MACHINE CONSTANTS FOR THE IBM 360/370 SERIES,
C     THE XEROX SIGMA 5/7/9, THE SEL SYSTEMS 85/86  AND
C     THE PERKIN ELMER (INTERdata) 7/32.
C
C     data RMACH(1) / Z00100000 /
C     data RMACH(2) / Z7FFFFFFF /
C     data RMACH(3) / Z3B100000 /
C     data RMACH(4) / Z3C100000 /
C     data RMACH(5) / Z41134413 /
C
C     MACHINE CONSTANTS FOR THE IBM PC
C
C     data SMALL(1) / 1.18E-38      /
C     data LARGE(1) / 3.40E+38      /
C     data RIGHT(1) / 0.595E-07     /
C     data DIVER(1) / 1.19E-07      /
C     data LOG10(1) / 0.30102999566 /
C
C     MACHINE CONSTANTS FOR THE IBM RS 6000
C
C     data RMACH(1) / Z'00800000' /
C     data RMACH(2) / Z'7F7FFFFF' /
C     data RMACH(3) / Z'33800000' /
C     data RMACH(4) / Z'34000000' /
C     data RMACH(5) / Z'3E9A209B' /
C
C     MACHINE CONSTANTS FOR THE INTEL i860
C
C     data RMACH(1) / Z'00800000' /
C     data RMACH(2) / Z'7F7FFFFF' /
C     data RMACH(3) / Z'33800000' /
C     data RMACH(4) / Z'34000000' /
C     data RMACH(5) / Z'3E9A209B' /
C
C     MACHINE CONSTANTS FOR THE PDP-10 (KA OR KI PROCESSOR)
C
C     data RMACH(1) / "000400000000 /
C     data RMACH(2) / "377777777777 /
C     data RMACH(3) / "146400000000 /
C     data RMACH(4) / "147400000000 /
C     data RMACH(5) / "177464202324 /
C
C     MACHINE CONSTANTS FOR PDP-11 FORTRAN SUPPORTING
C     32-BIT integerS (EXPRESSED IN integer AND OCTAL).
C
C     data SMALL(1) /    8388608 /
C     data LARGE(1) / 2147483647 /
C     data RIGHT(1) /  880803840 /
C     data DIVER(1) /  889192448 /
C     data LOG10(1) / 1067065499 /
C
C     data RMACH(1) / O00040000000 /
C     data RMACH(2) / O17777777777 /
C     data RMACH(3) / O06440000000 /
C     data RMACH(4) / O06500000000 /
C     data RMACH(5) / O07746420233 /
C
C     MACHINE CONSTANTS FOR PDP-11 FORTRAN SUPPORTING
C     16-BIT integerS  (EXPRESSED IN integer AND OCTAL).
C
C     data SMALL(1), SMALL(2) /   128,     0 /
C     data LARGE(1), LARGE(2) / 32767,    -1 /
C     data RIGHT(1), RIGHT(2) / 13440,     0 /
C     data DIVER(1), DIVER(2) / 13568,     0 /
C     data LOG10(1), LOG10(2) / 16282,  8347 /
C
C     data SMALL(1), SMALL(2) / O000200, O000000 /
C     data LARGE(1), LARGE(2) / O077777, O177777 /
C     data RIGHT(1), RIGHT(2) / O032200, O000000 /
C     data DIVER(1), DIVER(2) / O032400, O000000 /
C     data LOG10(1), LOG10(2) / O037632, O020233 /
C
C     MACHINE CONSTANTS FOR THE SILICON GRAPHICS
C
C     data RMACH(1) / Z'00800000' /
C     data RMACH(2) / Z'7F7FFFFF' /
C     data RMACH(3) / Z'33800000' /
C     data RMACH(4) / Z'34000000' /
C     data RMACH(5) / Z'3E9A209B' /
C
C     MACHINE CONSTANTS FOR THE SUN
C
C     data RMACH(1) / Z'00800000' /
C     data RMACH(2) / Z'7F7FFFFF' /
C     data RMACH(3) / Z'33800000' /
C     data RMACH(4) / Z'34000000' /
C     data RMACH(5) / Z'3E9A209B' /
C
C     MACHINE CONSTANTS FOR THE SUN
C     USING THE -r8 COMPILER OPTION
C
C     data RMACH(1) / Z'0010000000000000' /
C     data RMACH(2) / Z'7FEFFFFFFFFFFFFF' /
C     data RMACH(3) / Z'3CA0000000000000' /
C     data RMACH(4) / Z'3CB0000000000000' /
C     data RMACH(5) / Z'3FD34413509F79FF' /
C
C     MACHINE CONSTANTS FOR THE UNIVAC 1100 SERIES
C
C     data RMACH(1) / O000400000000 /
C     data RMACH(2) / O377777777777 /
C     data RMACH(3) / O146400000000 /
C     data RMACH(4) / O147400000000 /
C     data RMACH(5) / O177464202324 /
C
C     MACHINE CONSTANTS FOR THE Z80 MICROPROCESSOR
C
C     data SMALL(1), SMALL(2) /     0,    256/
C     data LARGE(1), LARGE(2) /    -1,   -129/
C     data RIGHT(1), RIGHT(2) /     0,  26880/
C     data DIVER(1), DIVER(2) /     0,  27136/
C     data LOG10(1), LOG10(2) /  8347,  32538/
C
C***FIRST EXECUTABLE STATEMENT  R1MACH
      if ((I.lt.1).or.(I.gt.5)) then
         write(*,*) 'I OUT OF BOUNDS'
      endif
C
      R1MACH = RMACH(I)
      return
C
      end

