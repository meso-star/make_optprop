#!/bin/bash
rm -f ./ecrad_output/output*.nc
rm -f ./ecrad_output/radiative_properties*.nc
../bin/ecrad ./ecrad_input/config.nam ./ecrad_input/clearsky.nc ./ecrad_output/output_nominal.nc > ecrad_run.log 2>&1
cp ./radiative_properties.nc ./ecrad_output/radiative_properties_nominal.nc
../bin/ecrad ./ecrad_input/config.nam ./ecrad_input/clearsky_nowater.nc ./ecrad_output/output_nowater.nc > ecrad_run.log 2>&1
cp ./radiative_properties.nc ./ecrad_output/radiative_properties_nowater.nc

iw=1
keep_going=true
while [ "$keep_going" = "true" ]
do
    if [ $iw -lt 10 ]
    then
	str=00$iw
    elif [ $iw -lt 100 ]
    then
	str=0$iw
    elif [ $iw -lt 1000 ]
    then
	str=$iw
    else
	echo "Too many points"
	exit 1
    fi
    file_in=./ecrad_input/water$str.nc
    if [ -e $file_in ]
    then
	file_out=./ecrad_output/output_water$str.nc
	file_rad=./ecrad_output/radiative_properties_water$str.nc
	../bin/ecrad ./ecrad_input/config.nam $file_in $file_out > ecrad_run.log 2>&1
	cp ./radiative_properties.nc $file_rad
	iw=$((iw+1))
    else
	keep_going=false
    fi
done # while keep_going=true

exit 0
